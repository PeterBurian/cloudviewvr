﻿using UnityEngine;

namespace CloudView.GUI
{
    public class Reticle : MonoBehaviour
    {
        public void LateUpdate()
        {
            transform.position = Camera.main.transform.position + Camera.main.transform.rotation * Vector3.forward * 2.0f;
            transform.LookAt(Camera.main.transform.position);
            transform.Rotate(0.0f, 180.0f, 0.0f);
        }
    }
}