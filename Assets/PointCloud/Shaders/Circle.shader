﻿Shader "Custom/Circle"
{
	Properties
	{
		_RealColor("realColor", Int) = 1
		_Scale("Scale", Range (1,40)) = 1
	}

    SubShader
    {
        Pass
        {
            CGPROGRAM

            #pragma target 5.0
            #pragma vertex vertexShader
            #pragma geometry geometryShader
            #pragma fragment fragmentShader

            #include "UnityCG.cginc"

            struct vertexOutput
            {
                float4 position : SV_POSITION;
                half4 color : TEXCOORD1;
            };

            // ------------------------------------------------------------------------------------
            // VertexShader
            // ------------------------------------------------------------------------------------

            //  Graphics.DrawProcedural(MeshTopology.Points, this.numOfInstancing)

            vertexOutput vertexShader(uint vertexID : SV_VertexID)
            {
                vertexOutput output;
                //output.position = float4(vertexID % 10, vertexID / 10, 0, 1);

				//output.position = float4(vertexID, vertexID, 0, 1);

                output.color = half4(1, 0, 0, 0);

                return output;
            }

            // ------------------------------------------------------------------------------------
            // GeometryShader
            // ------------------------------------------------------------------------------------

            [maxvertexcount(4)]
            void geometryShader(point vertexOutput input[1], inout TriangleStream<vertexOutput> outputStream)
            {
                vertexOutput output;
                float4 position = input[0].position;

                for (int x = 0; x < 2; x++)
                {
                    for (int y = 0; y < 2; y++)
                    {
                        output.position = position + float4(float2(x, y) * 0.5, 0, 0);
                        output.position = mul(UNITY_MATRIX_VP, output.position);
                        output.color = input[0] .color;
                        outputStream.Append(output);
                    }
                }
                outputStream.RestartStrip();
            }

			/*
			[maxvertexcount(128)]
            void geometryShader(point vertexOutput input[1], inout TriangleStream<vertexOutput> triStream)
            {
                vertexOutput OUT;
				OUT.color = input[0].color;
                float4 v = input[0].position;
                float4 v_prime = v + float4(0, 0, 0, 12);

                const float Pi = 3.141592;
                const int SEGMENT_COUNT = 32;
                for (int s = 0; s <= SEGMENT_COUNT; ++s) 
				{
                    float t1 = (float) s / SEGMENT_COUNT * 2 * Pi;
                    float t2 = (float) (s + 1) / SEGMENT_COUNT * 2 * Pi;

                    OUT.position = mul(UNITY_MATRIX_VP, v);
                    triStream.Append(OUT);

                    OUT.position = mul(UNITY_MATRIX_VP, v_prime - float4(cos(t1), sin(t1), 0, 0));
                    triStream.Append(OUT);

                    OUT.position = mul(UNITY_MATRIX_VP, v);
                    triStream.Append(OUT);

                    OUT.position = mul(UNITY_MATRIX_VP, v_prime - float4(cos(t2), sin(t2), 0, 0));
                    triStream.Append(OUT);
                }

                triStream.RestartStrip();
            }
			*/
            // ------------------------------------------------------------------------------------
            // FragmentShader
            // ------------------------------------------------------------------------------------
            
            fixed4 fragmentShader(vertexOutput input) : COLOR
            {
                return input.color;
            }

            ENDCG
        }
    }
}