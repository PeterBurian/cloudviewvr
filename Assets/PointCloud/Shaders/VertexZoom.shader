﻿Shader "Custom/VertexZoom"
{
	Properties
	{
		_Scale("Scale", Range (1,40)) = 1
	}

	SubShader
	{
		Pass
		{
			LOD 200

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			float _Scale = 1;
			
			struct VertexOutput
			{
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};

			VertexOutput vert(appdata_full input)
			{
				VertexOutput o;

				float4 positionV = UnityObjectToClipPos(input.vertex);
				positionV.xy *= float2(_Scale, _Scale);
				o.pos = positionV;
				o.col = float4(input.color.r, input.color.g, input.color.b, 0);
				
				return o;
			}

			float4 frag(VertexOutput o) : COLOR
			{
				return o.col;
			}

		ENDCG
		}
	}
}
