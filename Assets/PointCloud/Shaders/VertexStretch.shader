﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/VertexStretch"
{
	Properties
	{
		_RealColor("realColor", Int) = 1
		_Scale("Scale", Range (0,5)) = 1
		_MainTex ("Texture", 2D) = "white" {}
		_tX ("TranslateX", float) = 0
		_tY ("TranslateY", float) = 0
		_tZ ("TranslateZ", float) = 0
	}

	SubShader
	{
		Tags { "RenderType"="Opaque" }

		Pass
		{
			LOD 200

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"
			
			sampler2D _MainTex;
			float4 _MainTex_ST;

			int _RealColor;
			float _Scale = 1;
			float _tX,_tY,_tZ; 
			float _rX,_rY,_rZ;

			struct VertexOutput
			{
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};
			
			struct appdata
			{
				float4 vertex : POSITION;
				//float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				//float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
			};


			v2f vert(appdata v)
			{
				v2f o;
				
				float4x4 translateMatrix = float4x4(1,	0,	0,	_tX,
											 		0,	1,	0,	_tY,
									  				0,	0,	1,	_tZ,
									  				0,	0,	0,	1);

				float4x4 scaleMatrix 	= float4x4(_Scale,	0,		0,		0,
											 		0,		_Scale,	0,		0,
								  					0,		0,		_Scale,	0,
								  					0,		0,		0,		1);
				
				
				float angleX = radians(_rX);
				float c = cos(angleX);
				float s = sin(angleX);
				float4x4 rotateXMatrix	= float4x4(	1,	0,	0,	0,
											 		0,	c,	-s,	0,
								  					0,	s,	c,	0,
								  					0,	0,	0,	1);

				float angleY = radians(_rY);
				c = cos(angleY);
				s = sin(angleY);
				float4x4 rotateYMatrix	= float4x4(	c,	0,	s,	0,
											 		0,	1,	0,	0,
								  					-s,	0,	c,	0,
								  					0,	0,	0,	1);

				float angleZ = radians(_rZ);
				c = cos(angleZ);
				s = sin(angleZ);
				float4x4 rotateZMatrix	= float4x4(	c,	-s,	0,	0,
											 		s,	c,	0,	0,
								  					0,	0,	1,	0,
								  					0,	0,	0,	1);


  				float4 localVertexPos = v.vertex;

  				// NOTE: the order matters, try scaling first before translating, different results
  				float4 localTranslated = mul(translateMatrix,localVertexPos);
  				float4 localScaledTranslated = mul(localTranslated,scaleMatrix);
  				float4 localScaledTranslatedRotX = mul(localScaledTranslated,rotateXMatrix);
  				float4 localScaledTranslatedRotXY = mul(localScaledTranslatedRotX,rotateYMatrix);
  				float4 localScaledTranslatedRotXYZ = mul(localScaledTranslatedRotXY,rotateZMatrix);
				
				o.vertex = UnityObjectToClipPos(localScaledTranslated);

				//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			

			float4 frag(VertexOutput o) : COLOR
			{
				if (_RealColor == 1)
				{
					float intensity = o.col.a;

					float red = intensity >= 0.75 ? 1 : intensity < (0.75 && intensity > 0.5) ? (intensity - 0.5) / 0.25 : 0;
					float green = intensity > 0.75 ? (1 - intensity) / 0.75 : intensity >= 0.25 && intensity <= 0.75 ? 1 : intensity;
					float blue = intensity > 0.5 ? 0 : intensity <= 0.5 && intensity >= 0.25 ? (intensity - 0.5) / -0.25 : 1;

					o.col = float4(red, green, blue, 0);
				}
				else if(_RealColor == 2)
				{
					float intensity = o.col.a;

					float red = intensity > 0.5 ? 0 : intensity <= 0.5 && intensity >= 0.25 ? (intensity - 0.5) / -0.25 : 1;
					float blue = intensity >= 0.75 ? 1 : intensity < (0.75 && intensity > 0.5) ? (intensity - 0.5) / 0.25 : 0;
					float green = intensity > 0.75 ? (1 - intensity) / 0.75 : intensity >= 0.25 && intensity <= 0.75 ? 1 : intensity;

					o.col = float4(red, green, blue, 0);
				}
				return o.col;
			}

		ENDCG
		}
	}
}
