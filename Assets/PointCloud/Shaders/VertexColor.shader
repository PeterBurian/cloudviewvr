﻿Shader "Custom/VertexColor"
{
	Properties
	{
		_RealColor("realColor", Int) = 1
		_Scale("Scale", Range (1,40)) = 1
	}

	SubShader
	{
		Pass
		{
			CGPROGRAM
			
			//#pragma target 5.0
			#pragma vertex vert
			#pragma fragment frag
			//#pragma geometry geom

			#include "UnityCG.cginc"
			
			int _RealColor;
			float _Scale = 1;
			
			struct VertexOutput
			{
				float4 pos : SV_POSITION;
				float4 col : COLOR;
			};

			void vert(inout appdata_full v)
			{
				float4 positionV = UnityObjectToClipPos(v.vertex);
				v.vertex = positionV;
			}

			float4 frag(VertexOutput o) : COLOR
			{
				if (_RealColor == 1)
				{
					float intensity = o.col.a;

					float red = intensity >= 0.75 ? 1 : intensity < (0.75 && intensity > 0.5) ? (intensity - 0.5) / 0.25 : 0;
					float green = intensity > 0.75 ? (1 - intensity) / 0.75 : intensity >= 0.25 && intensity <= 0.75 ? 1 : intensity;
					float blue = intensity > 0.5 ? 0 : intensity <= 0.5 && intensity >= 0.25 ? (intensity - 0.5) / -0.25 : 1;

					o.col = float4(red, green, blue, 0);
				}
				else if(_RealColor == 2)
				{
					float intensity = o.col.a;

					float red = intensity > 0.5 ? 0 : intensity <= 0.5 && intensity >= 0.25 ? (intensity - 0.5) / -0.25 : 1;
					float blue = intensity >= 0.75 ? 1 : intensity < (0.75 && intensity > 0.5) ? (intensity - 0.5) / 0.25 : 0;
					float green = intensity > 0.75 ? (1 - intensity) / 0.75 : intensity >= 0.25 && intensity <= 0.75 ? 1 : intensity;

					o.col = float4(red, green, blue, 0);
				}
				return o.col;
			}
			/*
			[maxvertexcount(4)]
            void geometryShader(point VertexOutput input[1], inout TriangleStream<VertexOutput> outputStream)
            {
                VertexOutput output;
                float4 position = input[0].position;

                for (int x = 0; x < 2; x++)
                {
                    for (int y = 0; y < 2; y++)
                    {
                        output.pos = position + float4(float2(x, y) * 0.5, 0, 0);
                        output.pos = mul(UNITY_MATRIX_VP, output.position);
                        output.col = input[0] .col;
                        outputStream.Append(output);
                    }
                }
                outputStream.RestartStrip();
            }
			*/
		ENDCG
		}
	}
}
