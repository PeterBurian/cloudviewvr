﻿using Models;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.PointCloud.Scripts
{
    public class Clusterize
    {
        private List<CloudPoint> points;
        private Dictionary<Vector3, List<CloudPoint>> groups;
        private int count;
        private int maxGroupCount;
        private int maxItemsInGroup;
        private float groupRadius;

        public Clusterize(List<CloudPoint> pts)
        {
            points = new List<CloudPoint>(pts);
            count = points.Count;

            groups = new Dictionary<Vector3, List<CloudPoint>>();

            maxGroupCount = 350;
            maxItemsInGroup = 65000;
            groupRadius = 0.4f;
        }

        public Dictionary<Vector3, List<CloudPoint>> GetGroup()
        {
            return groups;
        }

        public void Run()
        {
            if (count > 0)
            {
                //Add the first point to group
                groups.Add(points[0].Vector, new List<CloudPoint>() { points[0] });

                for (int i = 1; i < points.Count; i++)
                {
                    float minDist = 0;
                    Vector3 minKey = Vector3.zero;

                    foreach (KeyValuePair<Vector3, List<CloudPoint>> item in groups)
                    {
                        float dist = Vector3.Distance(points[i].Vector, item.Key);

                        if (dist <= groupRadius && (minDist > dist || minDist == 0))
                        {
                            minDist = dist;
                            minKey = item.Key;
                        }
                    }

                    if (minDist == 0)
                    {
                        groups.Add(points[i].Vector, new List<CloudPoint>() { points[i] });
                    }
                    else
                    {
                        groups[minKey].Add(points[i]);

                        Vector3 average = Vector3.zero;
                        int piece = groups[minKey].Count;

                        for (int j = 0; j < piece; j++)
                        {
                            average += groups[minKey][j].Vector;
                        }

                        average /= piece;

                        List<CloudPoint> lst = new List<CloudPoint>(groups[minKey]);
                        groups.Remove(minKey);
                        groups.Add(average, lst);
                    }
                }
            }

            Test1();
        }

        private void Test1()
        {
            int count = 0;

            foreach (KeyValuePair<Vector3, List<CloudPoint>> item in groups)
            {
                count += item.Value.Count;
            }

            if (count == this.count)
            {
                Debug.Log("GOOD");
            }
        }
    }
}
