﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using Utilities;
using Database.Model;
using Handler;
using Database;
using MovementEffects;
using System.Collections.Generic;

#if UNITY_EDITOR

public class PointCloudManager : MonoBehaviour
{
    #region variables

    // File
    public string dataPath;
    private string filename;
    public Material matVertex;

    public float scale = 1;
    public bool invertYZ = false;
    public bool forceReload = false;

    public int numPoints;
    public int numPointGroups;
    private int limitPoints = 65000;

    // GUI
    private float progress = 0;
    private new string guiText;
    private bool loaded = false;

    // PointCloud
    private GameObject pointCloud;

    private Vector3[] points;
    private Color[] colors;

    #endregion

    public void Start()
    {
        // Create Resources folder
        createFolders();

        // Get Filename
        filename = Path.GetFileName(dataPath);

        loadScene();
    }

    private void createFolders()
    {
        if (!Directory.Exists(Paths.Resources()))
        {
            UnityEditor.AssetDatabase.CreateFolder("Assets", "Resources");
        }

        if (!Directory.Exists(Paths.PointCloudMeshes()))
        {
            UnityEditor.AssetDatabase.CreateFolder("Assets/Resources", "PointCloudMeshes");
        }
    }

    private void loadScene()
    {
        String dirPath = Paths.PointCloudMeshes() + filename;
        // Check if the PointCloud was loaded previously

        if (!Directory.Exists(dirPath))
        {
            UnityEditor.AssetDatabase.CreateFolder("Assets/Resources/PointCloudMeshes", filename);

            loadPointCloud();
        }
        else if (forceReload)
        {
            UnityEditor.FileUtil.DeleteFileOrDirectory(Paths.PointCloudMeshes() + filename);
            UnityEditor.AssetDatabase.Refresh();
            UnityEditor.AssetDatabase.CreateFolder("Assets/Resources/PointCloudMeshes", filename);

            loadPointCloud();
        }
        else
        {
            // Load stored PointCloud
            loadStoredMeshes();
        }
    }

    private void loadPointCloud()
    {
        // Check what file exists
        String filePath = GetFilepathWithExtension();

        if (File.Exists(filePath))
        {
            // load off
            IEnumerator<float> _loadOFF = _LoadOFF(dataPath + ".bin");
            Timing.RunCoroutine(_loadOFF);
        }
        else
        {
            Debug.LogError("File '" + dataPath + "' could not be found");
        }
    }

    private String GetFilepathWithExtension()
    {
        String ext = Path.GetExtension(dataPath);
        String filePath = Path.Combine(Application.dataPath, dataPath);

        if (String.IsNullOrEmpty(ext))
        {
            return filePath + ".off";
        }
        else
        {
            return filePath;
        }
    }

    // Load stored PointCloud
    private void loadStoredMeshes()
    {
        Debug.Log("Using previously loaded PointCloud: " + filename);

        GameObject pointCloud = Instantiate(Resources.Load("PointCloudMeshes/" + filename)) as GameObject;

        loaded = true;
    }

    // Start Coroutine of reading the points from the OFF file and creating the meshes
    private IEnumerator<float> _LoadOFF(string dPath)
    {
        System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
        stopWatch.Start();

        // Read file
        String fPath = Path.Combine(Application.dataPath, dPath);

        if (File.Exists(fPath))
        {
            bool isBinary = true;

            if (isBinary)
            {
                numPoints = 18001932;

                points = new Vector3[numPoints];
                colors = new Color[numPoints];

                using (FileStream fs = File.Open(fPath, FileMode.Open))
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        for (int i = 0; i < numPoints; i++)
                        {
                            float x = br.ReadSingle();
                            float y = br.ReadSingle();
                            float z = br.ReadSingle();

                            points[i] = new Vector3(x * scale, y * scale, z * scale);

                            float r = br.ReadSingle();
                            float g = br.ReadSingle();
                            float b = br.ReadSingle();

                            colors[i] = new Color(r, g, b);

                            progress = i * 1.0f / (numPoints - 1) * 1.0f;
                            if (i % Mathf.FloorToInt(numPoints / 20) == 0)
                            {
                                guiText = i.ToString() + " out of " + numPoints.ToString() + " loaded";
                                yield return Timing.WaitForOneFrame;
                            }
                        }
                    }
                }
            }
            else
            {
                StreamReader sr = new StreamReader(fPath);
                sr.ReadLine(); // OFF
                String[] buffer = sr.ReadLine().Split(); // nPoints, nFaces

                numPoints = int.Parse(buffer[0]);
                points = new Vector3[numPoints];
                colors = new Color[numPoints];

                for (int i = 0; i < numPoints; i++)
                {
                    buffer = sr.ReadLine().Split();

                    if (!invertYZ)
                    {
                        points[i] = new Vector3(float.Parse(buffer[0]) * scale, float.Parse(buffer[1]) * scale, float.Parse(buffer[2]) * scale);
                    }
                    else
                    {
                        points[i] = new Vector3(float.Parse(buffer[0]) * scale, float.Parse(buffer[2]) * scale, float.Parse(buffer[1]) * scale);
                    }


                    if (buffer.Length >= 5)
                    {
                        colors[i] = new Color(int.Parse(buffer[3]) / 255.0f, int.Parse(buffer[4]) / 255.0f, int.Parse(buffer[5]) / 255.0f);
                    }
                    else
                    {
                        colors[i] = Color.cyan;
                    }

                    // Relocate Points near the origin
                    //calculateMin(points[i]);

                    // GUI
                    progress = i * 1.0f / (numPoints - 1) * 1.0f;

                    if (i % Mathf.FloorToInt(numPoints / 20) == 0)
                    {
                        guiText = i.ToString() + " out of " + numPoints.ToString() + " loaded";
                        yield return Timing.WaitForOneFrame;
                    }
                }
            }

            // Instantiate Point Groups
            numPointGroups = Mathf.CeilToInt(numPoints * 1.0f / limitPoints * 1.0f);

            pointCloud = new GameObject(filename);

            String datafolderPath = Path.Combine(Application.persistentDataPath, "Data1");
            if (Directory.Exists(datafolderPath))
            {
                Directory.Delete(datafolderPath, true);
                Directory.CreateDirectory(datafolderPath);
            }

            for (int i = 0; i < numPointGroups - 1; i++)
            {
                InstantiateMesh(i, limitPoints);

                if (i % 10 == 0)
                {
                    guiText = i.ToString() + " out of " + numPointGroups.ToString() + " PointGroups loaded";
                    yield return Timing.WaitForOneFrame;
                }
            }
            InstantiateMesh(numPointGroups - 1, numPoints - (numPointGroups - 1) * limitPoints);

            loaded = true;
        }
        else
        {
            loaded = false;
            yield return Timing.WaitForOneFrame;
        }

        stopWatch.Stop();
        UnityEngine.Debug.Log("Full: " + stopWatch.ElapsedMilliseconds.ToString());
    }

    private void InstantiateMesh(int meshInd, int nPoints)
    {
        String meshName = filename + meshInd;
        CreateMeshGameobject(meshName, meshInd, nPoints);
    }

    private GameObject CreateMeshGameobject(String meshName, int meshInd, int nPoints)
    {
        GameObject pointGroup = new GameObject(meshName);
        pointGroup.AddComponent<MeshFilter>();
        pointGroup.AddComponent<MeshRenderer>();
        pointGroup.GetComponent<Renderer>().material = matVertex;

        pointGroup.GetComponent<MeshFilter>().mesh = CreateMesh(meshInd, nPoints, limitPoints, meshName, pointGroup.transform);
        pointGroup.transform.parent = pointCloud.transform;

        return pointGroup;
    }

    private Mesh CreateMesh(int id, int nPoints, int limitPoints, String meshName, Transform transform)
    {
        Mesh mesh = new Mesh();

        Vector3[] meshVerts = new Vector3[nPoints];
        int[] indecies = new int[nPoints];
        Color[] myColors = new Color[nPoints];

        for (int i = 0; i < nPoints; ++i)
        {
            meshVerts[i] = points[id * limitPoints + i];
            indecies[i] = i;
            myColors[i] = colors[id * limitPoints + i];
        }

        SaveMeshPoints2File(meshVerts, myColors, meshName);

        mesh.vertices = meshVerts;

        mesh.colors = myColors;
        mesh.SetIndices(indecies, MeshTopology.Points, 0);
        mesh.uv = new Vector2[nPoints];

        mesh.normals = new Vector3[nPoints];


        AddItems2Db(mesh, meshName, transform);

        return mesh;
    }

    private void SaveMeshPoints2File(Vector3[] points, Color[] colors, String meshName)
    {
        String datafolderPath = Path.Combine(Application.persistentDataPath, "Data");
        String meshPath = Path.Combine(datafolderPath, meshName + ".bin");

        //BinaryHandler.Write2Bin(meshPath, points, colors);
        BinaryHandler.Write2Bin_15(meshPath, points, colors);
    }

    private void AddItems2Db(Mesh messh, String meshName, Transform transform)
    {
        Vector minV = CreateVector(transform.TransformPoint(messh.bounds.min));
        Vector maxV = CreateVector(transform.TransformPoint(messh.bounds.max));
        Vector center = CreateVector(transform.TransformPoint(messh.bounds.center));

        SqliteAdapter.Insert<Vector>(minV);
        SqliteAdapter.Insert<Vector>(maxV);
        SqliteAdapter.Insert<Vector>(center);

        MeshItem meshItem = new MeshItem();
        meshItem.FileName = meshName;
        meshItem.Min = minV.id;
        meshItem.Max = maxV.id;
        meshItem.Center = center.id;
        meshItem.id = Guid.NewGuid().ToString();

        SqliteAdapter.Insert<MeshItem>(meshItem);
    }

    private Vector CreateVector(Vector3 dat)
    {
        Vector vec = new Vector();
        vec.x = dat.x;
        vec.y = dat.y;
        vec.z = dat.z;
        vec.id = Guid.NewGuid().ToString();

        return vec;
    }

    private void OnGUI()
    {
        if (!loaded)
        {
            GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2, 400.0f, 20));
            GUI.Box(new Rect(0, 0, 200.0f, 20.0f), guiText);
            GUI.Box(new Rect(0, 0, progress * 200.0f, 20), "");
            GUI.EndGroup();
        }
    }
}

#endif