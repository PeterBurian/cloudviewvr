﻿using System.Diagnostics;
using Utilities;

namespace CloudView.Test
{
    public class Watch : Stopwatch
    {
        public Watch() : base()
        {
            StartWatch();
        }

        public void StartWatch()
        {
            Start();
        }

        public void StopWatch()
        {
            Stop();

            UnityEngine.Debug.Log("Time: " + base.ElapsedMilliseconds);
            Log.Logger.Print("Time: " + base.ElapsedMilliseconds);
        }

        public void ResetWatch()
        {
            Reset();
            Start();
        }
    }
}
