﻿Shader "Custom/Unlit/SimpleInstancing"
{
	Properties
	{
		_Scale("Scale", Range (1,10)) = 10
	}

    SubShader
    {
        Pass
        {
            CGPROGRAM

            #pragma target 5.0
            #pragma vertex vertexShader
            #pragma geometry geometryShader
            #pragma fragment fragmentShader

            #include "UnityCG.cginc"
		
			int _Scale;

	
            struct vertexOutput
            {
                float4 position : SV_POSITION;
                half4 color : TEXCOORD1;
            };

            vertexOutput vertexShader(uint vertexID : SV_VertexID)
            {
                vertexOutput output;
                output.position = float4(vertexID % 10, vertexID / 10, 0, 1);

                half value = vertexID % 10 / 10.0;
                output.color = half4(value, value, value, 1);

                return output;
            }
/*
            [maxvertexcount(4)]
            void geometryShader(point vertexOutput input[1], inout TriangleStream<vertexOutput> outputStream)
            {
                vertexOutput output;
                float4 position = input[0].position;
		
				float scale = _Scale / 10.0 * 0.5;

                for (int x = 0; x < 2; x++)
                {
                    for (int y = 0; y < 2; y++)
                    {
                        output.position = position + float4(float2(x - 0.5, y - 0.5) * scale, 0, 0);	//2.:Z; 
                        output.position = mul(UNITY_MATRIX_VP, output.position);
			
                        output.color = input[0].color;
                        outputStream.Append(output);
                    }
                }

                outputStream.RestartStrip();
            }		
*/

			[maxvertexcount(64)]
            void geometryShader(point vertexOutput input[1], inout TriangleStream<vertexOutput> outputStream)
			{
				double PI = 3.141592654;

				vertexOutput output;
                float4 position = input[0].position;

				double radius = 2.0f;
				int step = 64;
				double angle = 2 * PI / step;

				float scale = _Scale / 10.0 * 0.5;

				for (int i = 0; i <= step; i++)
				{
					double x = position.x + cos(i * angle) * scale;
					double y = position.y + sin(i * angle) * scale;

					output.position = position + float4(float2(x, y), 0, 0);
                    output.position = mul(UNITY_MATRIX_VP, output.position);
			
                    output.color = input[0].color;
                    outputStream.Append(output);
			
/*
					vertexOutput origo;
					origo.position = mul(UNITY_MATRIX_VP, input[0].position);
					origo.color = output.color;
					
					outputStream.Append(origo);
					*/
				}
				
				outputStream.RestartStrip();
			}		

            fixed4 fragmentShader(vertexOutput input) : COLOR
            {
                return input.color;
            }

            ENDCG
        }
    }
}