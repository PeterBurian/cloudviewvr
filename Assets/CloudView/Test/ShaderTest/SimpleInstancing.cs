﻿using CloudView.Test;
using System.Runtime.InteropServices;
using UnityEngine;

public class SimpleInstancing : MonoBehaviour
{
    public Shader simpleInstancingShader;

    [Range(0, 100)]
    public int numOfInstancing;

    [SerializeField, Range(1, 10)]
    private int scale;

    public Material material;
    private ComputeBuffer vertexBuffer;

    void Start()
    {
        scale = 10;

        Vector3[] vertices = new Vector3[] { new Vector3(0, 0, 0) };
        this.vertexBuffer = new ComputeBuffer(1, Marshal.SizeOf(typeof(Vector3)));
        this.vertexBuffer.SetData(vertices);

        this.material = new Material(this.simpleInstancingShader);
        this.material.SetBuffer("_VertexBuffer", this.vertexBuffer);

        //Draw();
    }

    void OnRenderObject()
    {
        this.material.SetPass(0);
        Graphics.DrawProcedural(MeshTopology.Points, this.numOfInstancing);
    }

    void OnDestroy()
    {
        DestroyImmediate(this.material);
        this.vertexBuffer.Release();
    }

    private void OnValidate()
    {
        numOfInstancing = 1;

        if (material != null)
        {
            material.SetInt("_Scale", scale);
        }
    }

    private void Draw()
    {
        BasicTests test = new BasicTests();
        Vector2 origo = Vector2.zero;
        float radius = 4.0f;
        int step = 32;
        float angle = 2 * Mathf.PI / step;

        for (int i = 0; i < step; i++)
        {
            float x = origo.x + Mathf.Cos(i * angle) * radius;
            float y = origo.y + Mathf.Sin(i * angle) * radius;

            Vector2 pt = new Vector2(x, y);
            test.DrawSphere(pt);
        }
    }
}