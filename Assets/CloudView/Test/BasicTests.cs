﻿using System.Collections.Generic;
using UnityEngine;

namespace CloudView.Test
{
    public class BasicTests
    {
        public void DrawSpheres(List<Vector3> positions)
        {
            for (int i = 0; i < positions.Count; i++)
            {
                DrawSphere(positions[i]);
            }
        }

        public void DrawSphere(Vector3 position)
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            go.transform.localEulerAngles = Vector3.zero;
            go.transform.localScale = Vector3.one;
            go.transform.position = position;
        }

        public void DrawLine(Vector3 end)
        {
            DrawLine(Vector3.zero, end);
        }

        public void DrawLine(Vector3 start, Vector3 end)
        {
            Debug.DrawLine(start, end, Color.red);
        }

        private void DrawAxis()
        {
            Vector3 origo = new Vector3(-41.007491f, 1.871948f, -0.992718f);
            Vector3 vx = new Vector3(0.969887f, -0.243402f, -0.008649f);
            Vector3 vy = new Vector3(0.243496f, 0.969826f, 0.012182f);
            Vector3 vz = new Vector3(0.005423f, -0.013921f, 0.999888f);

            Debug.DrawRay(origo, vx, Color.red, 300);
            Debug.DrawRay(origo, vy, Color.green, 300);
            Debug.DrawRay(origo, vz, Color.blue, 300);
        }
    }
}
