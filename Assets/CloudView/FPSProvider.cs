﻿using Assets.CloudView.Sensors;
using CloudView.Events;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

namespace Assets.CloudView
{
    public class FPSProvider : MonoBehaviour, IPedometer, IMarch
    {
        private FirstPersonController fpsGo;

        private void Awake()
        {
            fpsGo = GameObject.FindObjectOfType<FirstPersonController>();

            EventBus.Instance.Register<IPedometer>(gameObject);
            EventBus.Instance.Register<IMarch>(gameObject);
        }

        private void OnDestroy()
        {
            EventBus.Instance.Unregister(gameObject);
        }

        public void StepTaken()
        {
            fpsGo.StepTaken();
        }

        public void MovedByFPS()
        {
            EventBus.Instance.Post<ILoader>((e, f) => e.MovedByFPSC());
        }

        public void Lift(bool isUp)
        {
            float elevation = isUp ? 1f : -1f;
            fpsGo.Lift(elevation);
        }

        public void TeleportNext(Vector3 position)
        {
            fpsGo.SetCenter(position);
        }
    }
}