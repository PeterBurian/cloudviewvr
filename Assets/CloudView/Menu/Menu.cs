﻿using CloudView.Controller;
using CloudView.Enums;
using CloudView.Events;
using CloudView.Settings;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CloudView.Menu
{
    public class Menu : MonoBehaviour, IControllPressed
    {
        public Text txtDensity;
        public Text txtMode;

        public delegate void GetVal();
        
        private static int density;

        public void Start()
        {
            EventBus.Instance.Register<IControllPressed>(gameObject);
            UserSettings settings = new UserSettings();
            density = settings.GetDensity();
            settings.GetCurrentMode();

            DisplayDensity();
            DisplayMode();
        }

        public void ChangeToScene(int scnene)
        {
            SceneManager.LoadScene(scnene);
        }

        public void A_Pressed()
        {
            ChangeDensity(true);
        }

        public void B_Pressed()
        {
            ChangeDensity(false);
        }

        public void C_Pressed()
        {
            OnModeChanged();
        }

        public void D_Pressed()
        {
            throw new System.NotImplementedException();
        }

        public void On_D_Minus()
        {
            ChangeDensity(false);
        }

        public void On_D_Plus()
        {
            ChangeDensity(true);
        }

        public void On_M_Minus()
        {
            ModeChange(new UserSettings().Change2PrevMode);
        }

        public void On_M_Plus()
        {
            OnModeChanged();
        }

        private void OnModeChanged()
        {
            ModeChange(new UserSettings().Change2NextMode);
        }

        private void ModeChange(GetVal methd)
        {
            methd();
            DisplayMode();
        }

        private void ChangeDensity(bool isIncrease)
        {
            if (isIncrease && density < 100)
            {
                density += 10;
                OnDensityChanged();
            }
            else if (!isIncrease && density > 0)
            {
                density -= 10;
                OnDensityChanged();
            }
        }

        private void OnDensityChanged()
        {
            DisplayDensity();
            Save2PlayerPrefs(density);
        }

        private void DisplayDensity()
        {
            StringBuilder builder = new StringBuilder("");
            builder.Append(density);
            builder.Append("%");
            txtDensity.text = builder.ToString();
        }

        private void DisplayMode()
        {
            UserSettings settings = new UserSettings();
            Combo currentType = (Combo)settings.GetCurrentMode();

            StringBuilder builder = new StringBuilder();
            builder.Append(currentType.ToString());

            txtMode.text = builder.ToString();
        }

        private void Save2PlayerPrefs(int densityInPercent)
        {
            UserSettings settings = new UserSettings();
            settings.SetDensity(densityInPercent);
        }
    }
}