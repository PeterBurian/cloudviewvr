﻿#define ENABLE_WATCH

using Assets.CloudView.Preload;
using CloudView.Controller;
using CloudView.Events;
using CloudView.Settings;
using CloudView.Test;
using Handler;
using Models;
using MovementEffects;
using Scripts.Enums;
using Scripts.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEngine;
using Utilities;
using Utilities.Extensions;

namespace Assets.CloudView
{
    public partial class Loader : MonoBehaviour
    {
        public Material matVertex;
        public bool differentDensities = true;

        [Range(1, 10)]
        public int ImageDensity;

        private MeshBuilder builder;
        private Dictionary<String, List<CloudPoint>> cached;

        private Camera cam;
        private Plane[] planes;

        private bool showGui = true;
        private float progress = 0;
        private new string guiText;

        private Watch watch;

        private float Rate
        {
            get
            {
                return (10 * ImageDensity) / 100.0f;
            }
        }

        public void Awake()
        {
            GetDensity();

            EventBus.Instance.Register<IControllPressed>(gameObject);
            EventBus.Instance.Register<ILoader>(gameObject);
        }

        public void OnDestroy()
        {
            EventBus.Instance.Unregister(this.gameObject);
        }

        public void LoadMeshes()
        {

#if ENABLE_WATCH
            watch = new Watch();
#else
            watch = null;
#endif
            try
            {
                builder = new MeshBuilder(matVertex);
                MeshFacade.GetFacade().Load();
            }
            catch (Exception ex)
            {
                Log.Logger.Print(ex.ToString());
            }

            ReloadWithoutReset();
        }

        public void Reload()
        {
            if (watch != null)
            {
                watch.ResetWatch();
            }

            ReloadWithoutReset();
        }

        public void ReloadWithoutReset()
        {
            GetPlanes();
            showGui = true;

            Load();
        }

        private void GetPlanes()
        {
            cam = Camera.main;
            planes = GeometryUtility.CalculateFrustumPlanes(cam);
        }

        private bool IsPosInBound(Bounds bounds)
        {
            GameObject go = GameObject.Find("FPSController");
            Vector3 pos = go.transform.position;
            return bounds.Contains(pos);
        }

        private void Load()
        {
            cached = new Dictionary<String, List<CloudPoint>>();

            if (Directory.Exists(Paths.Data()))
            {
                LoadAll();
            }
        }

        private void LoadAll()
        {
            int visibleItems = 0;

            //Set all items in list to false
            MeshFacade.GetFacade().ResetUtils();

            Dictionary<String, Density> parts = GetNewVisibleFiles(out visibleItems, true);

            if (differentDensities)
            {
                // parts contains the new items -> have to load it
                // Get the changed density -> we have to delete them and load it again (visible = true, isDensityChanged=true)
                // Preload all points
                // DeleteInvisibles

                Dictionary<String, Density> reloadables = MeshFacade.GetFacade().GetChangedDensitiesPath();

                Dictionary<String, Density>[] items = new Dictionary<string, Density>[2];
                items[0] = parts;
                items[1] = reloadables;

                DeleteInvisibles();

                PreloadPoints(items);

                IEnumerator<float> _coroutine = _LoadMeshesWithDensityCr(items);
                Timing.RunCoroutine(_coroutine);
            }
            else
            {
                List<String> files = parts.Keys.ToList();
                PreloadPoints(files);

                DeleteInvisibles();

                IEnumerator<float> coroutine = _LoadMeshes(files);
                Timing.RunCoroutine(coroutine);
            }
        }

        private void DeleteInvisibles()
        {
            List<String> delables = MeshFacade.GetFacade().GetInvisibles(true);

            for (int i = 0; i < delables.Count; i++)
            {
                IEnumerator<float> coroutine = _DeleteMesh(delables[i]);
                Timing.RunCoroutine(coroutine);
            }
        }

        private Dictionary<String, Density> GetVisibleFiles()
        {
            int visibleItems = 0;
            return GetNewVisibleFiles(out visibleItems);
        }

        private Dictionary<String, Density> GetNewVisibleFiles(out int visibleItems, bool enableAll = false)
        {
            Dictionary<String, Density> newItems = new Dictionary<String, Density>();
            int counter = 0;

            String folder = Paths.Data();

            GameObject controller = GameObject.Find("FPSController");
            Vector3 currentPlace = controller.transform.localPosition;

            Vector3[] frustumCorners = FrustumTest.GetFrustumCorners(cam);

            MeshFacade.GetFacade().Loop(delegate (MeshModel model)
            {
                if (enableAll || CheckVisibility(model, frustumCorners))
                {
                    counter++;

                    Density density = Density.NONE;

                    if (differentDensities)
                    {
                        density = DensityHandler.GetDensityFromCenter(currentPlace, model.GetBounds().center);
                    }

                    if (model.SetVisible(density))
                    {
                        String path = Path.Combine(folder, model.fileName + ".bin");
                        newItems.Add(path, density);
                    }
                }
            });

            visibleItems = counter;
            UnityEngine.Debug.Log("Visible meshes: " + counter);

            return newItems;
        }

        #region Visibilty check

        private bool CheckVisibilityWithUnity(Bounds bounds)
        {
            // Have to change Y and z axis !!!
            //Planes ordering: [0] = Left, [1] = Right, [2] = Down, [3] = Up, [4] = Near, [5] = Far

            if (GeometryUtility.TestPlanesAABB(planes, bounds))
            {
                return true;
            }
            else
            {
                //Test partially contains
                //TestPartiallyContains(bounds);
            }
            return false;
        }

        private bool CheckVisibility(MeshModel model, Vector3[] frustum)
        {
            Vector2[] boundsBottom = model.GetBoundsBottomVectors();

            Vector2[] frustumBottom = new Vector2[4]
            {
                new Vector2(frustum[0].x, frustum[0].z),
                new Vector2(frustum[2].x, frustum[2].z),
                new Vector2(frustum[4].x, frustum[4].z),
                new Vector2(frustum[6].x, frustum[6].z)
            };

            if (GeometryExtension.ConvexPolygonOverlap(frustumBottom, boundsBottom))
            {
                return true;
            }
            return false;
        }


        #endregion

        private void PreloadPoints(Dictionary<String, Density>[] items)
        {
            Thread thread = new Thread(delegate ()
            {
                for (int i = 0; i < items.Length; i++)
                {
                    CacheLimit cLimit = new CacheLimit(items[i].Count);
                    int from = cLimit.GetFrom4Preload();

                    if (from > 0 && from < items[i].Count)
                    {
                        for (int j = from; j < items[i].Count; j++)
                        {
                            var pair = items[i].ElementAt(j);

                            KeyValuePair<String, List<CloudPoint>> result = builder.ReadPoints(pair.Key, pair.Key, (int)pair.Value);
                            cached.Add(result.Key, result.Value);
                        }
                    }
                }
            });
            thread.Start();
        }

        private void PreloadPoints(List<String> parts)
        {
            CacheLimit cLimit = new CacheLimit(parts.Count);
            int from = cLimit.GetFrom4Preload();

            if (from > 0 && from < parts.Count)
            {
                Thread thread = new Thread(delegate ()
                {
                    // Storing cache keys before reading the points
                    // This way we won't use the same file
                    for (int i = from; i < parts.Count; i++)
                    {
                        cached[parts[i]] = new List<CloudPoint>();
                    }

                    for (int i = from; i < parts.Count; i++)
                    {
                        String path = parts[i];
                        KeyValuePair<String, List<CloudPoint>> pair = builder.ReadPoints(path, path, Rate);
                        cached[path] = pair.Value;
                    }
                });
                thread.Start();
            }
        }

        private Dictionary<Density, List<String>> GetDensities()
        {
            Dictionary<Density, List<String>> groups = new Dictionary<Density, List<string>>();

            groups.Add(Density.FULL, new List<String>());
            groups.Add(Density.HIGH, new List<String>());
            groups.Add(Density.MEDIUM, new List<String>());
            groups.Add(Density.LOW, new List<String>());

            GameObject controller = GameObject.Find("FPSController");
            if (controller != null)
            {
                Vector3 pos = controller.transform.localPosition;
                SortedList<float, String> files = MeshFacade.GetFacade().GetVisiblesByDistance(pos);
                String folder = Paths.Data();

                foreach (KeyValuePair<float, String> item in files)
                {
                    String path = Path.Combine(folder, item.Value + ".bin");

                    Density density = DensityHandler.GetDensityFromDistance(item.Key);
                    groups[density].Add(path);
                }
            }
            return groups;
        }

        #region Load meshes

        private IEnumerator<float> _LoadMeshesWithDensityCr(Dictionary<String, Density>[] groups)
        {
            int numPoints = groups[0].Count + groups[1].Count;
            int counter = 0;

            for (int i = 0; i < groups.Length; i++)
            {
                foreach (KeyValuePair<String, Density> pair in groups[i])
                {
                    String key = pair.Key;
                    if (cached.ContainsKey(key))
                    {
                        builder.ReadPoints(cached[key], key, i == 1);
                        cached.Remove(key);
                    }
                    else
                    {
                        builder.ReadPoints(key, (int)pair.Value, i == 1);
                    }

                    counter += 1;
                    progress = counter * 1.0f / (numPoints - 1) * 1.0f;

                    int devide = Mathf.FloorToInt(numPoints / 15);

                    if (devide != 0 && counter % devide == 0)
                    {
                        float percent = (counter / (float)numPoints) * 100.0f;
                        guiText = percent + "%";
                        yield return Timing.WaitForOneFrame;
                    }
                }
            }

            if (watch != null)
            {
                watch.StopWatch();
            }

            cached.Clear();
            showGui = false;

            yield return Timing.WaitForOneFrame;
        }

        private IEnumerator<float> _LoadMeshes(List<String> parts)
        {
            for (int i = 0; i < parts.Count; i++)
            {
                String part = parts[i];

                if (cached.ContainsKey(part))
                {
                    builder.ReadPoints(cached[part], part);
                    cached.Remove(part);
                }
                else
                {
                    builder.ReadPoints(parts[i], Rate, false);
                }

                int numPoints = parts.Count - 1;
                progress = i * 1.0f / (numPoints - 1) * 1.0f;

                int devide = Mathf.FloorToInt(numPoints / 20);

                if (devide != 0 && i % devide == 0)
                {
                    float percent = (i / (float)numPoints) * 100.0f;
                    guiText = percent + "%";
                    yield return Timing.WaitForOneFrame;
                }
            }

            if (watch != null)
            {
                watch.StopWatch();
            }

            cached.Clear();
            showGui = false;

            yield return Timing.WaitForOneFrame;
        }

        private IEnumerator<float> LoadSingleMesh(KeyValuePair<String, List<CloudPoint>> item)
        {
            builder.ReadPoints(item.Value, item.Key);
            yield return Timing.WaitForOneFrame;
        }

        #endregion

        private IEnumerator<float> _DeleteMesh(String fileName)
        {
            GameObject go = GameObject.Find(fileName);

            if (go != null)
            {
                GameObject.Destroy(go);
            }

            yield return Timing.WaitForOneFrame;
        }

        public void GetDensity()
        {
            UserSettings settings = new UserSettings();
            float density = settings.GetDensity() / 10.0f;

            ImageDensity = (int)density;
        }

        private void OnGUI()
        {
            if (showGui)
            {
                GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2, 400.0f, 20));

                GUIStyle style= new GUIStyle("box");
                style.fontSize = 15;

                GUI.Box(new Rect(0, 0, 200.0f, 20.0f), guiText, style);
                GUI.Box(new Rect(0, 0, progress * 200.0f, 20), "");

                GUI.EndGroup();
            }
        }
    }
}