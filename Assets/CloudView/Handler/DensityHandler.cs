﻿using Scripts.Enums;
using UnityEngine;

namespace Handler
{
    public static class DensityHandler
    {
        // I found out some density group for the points
        /*
         - < 2 full density
         - 6 > & < 6m --- half density (2)
         - 6 > & < 10 (4)
         - > 10 (6)
         */

        public static Density GetDensityFromCenter(Vector3 from, Vector3 boundsCenter)
        {
            float dist = Vector3.Distance(from, boundsCenter);
            return GetDensityFromDistance(dist);
        }

        public static Density GetDensityFromCenter(Vector3 center)
        {
            GameObject controller = GameObject.Find("FPSController");
            if (controller != null)
            {
                Vector3 pos = controller.transform.localPosition;
                float dist = Vector3.Distance(pos, center);

                return GetDensityFromDistance(dist);
            }
            return Density.NONE;
        }

        public static Density GetDensityFromDistance(float dist)
        {
            if (dist <= 2)
            {
                return Density.FULL;
            }
            else if (dist > 2 && dist < 6)
            {
                return Density.HIGH;
            }
            else if (dist >= 6 && dist <= 10)
            {
                return Density.MEDIUM;
            }
            else if (dist > 10)
            {
                return Density.LOW;
            }
            else
            {
                return Density.NONE;
            }
        }
    }
}
