﻿using Models;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Handler
{
    public static class BinaryHandler
    {
        public static void Write2Bin(String filePath, Vector3[] points, Color[] colors)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.OpenOrCreate)))
            {
                for (int i = 0; i < points.Length; i++)
                {
                    writer.Write(points[i].x);
                    writer.Write(points[i].y);
                    writer.Write(points[i].z);

                    writer.Write(colors[i].r);
                    writer.Write(colors[i].g);
                    writer.Write(colors[i].b);
                }
            }
        }

        public static void Write2Bin_15(String filePath, Vector3[] points, Color[] colors)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.OpenOrCreate)))
            {
                for (int i = 0; i < points.Length; i++)
                {
                    writer.Write(points[i].x);
                    writer.Write(points[i].y);
                    writer.Write(points[i].z);

                    writer.Write((byte)(colors[i].r * 255.0f));
                    writer.Write((byte)(colors[i].g * 255.0f));
                    writer.Write((byte)(colors[i].b * 255.0f));
                }
            }
        }

        public static void Write2Bin_19(String filePath, Vector3[] points, float intensity, Color[] colors)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.OpenOrCreate)))
            {
                for (int i = 0; i < points.Length; i++)
                {
                    writer.Write(points[i].x);
                    writer.Write(points[i].y);
                    writer.Write(points[i].z);

                    writer.Write(intensity);

                    writer.Write((byte)(colors[i].r * 255.0f));
                    writer.Write((byte)(colors[i].g * 255.0f));
                    writer.Write((byte)(colors[i].b * 255.0f));
                }
            }
        }

        public static void Write2Bin_19(String filePath, CloudPoint[] points, bool isInvert)
        {
            using (BinaryWriter binWriter = new BinaryWriter(File.Open(filePath, FileMode.OpenOrCreate)))
            {
                for (int i = 0; i < points.Length; i++)
                {
                    CloudPoint point = points[i];

                    binWriter.Write(point.Vector.x);

                    if (isInvert)
                    {
                        binWriter.Write(point.Vector.z);
                        binWriter.Write(point.Vector.y);
                    }
                    else
                    {
                        binWriter.Write(point.Vector.y);
                        binWriter.Write(point.Vector.z);
                    }

                    binWriter.Write(point.Intensity);

                    binWriter.Write((byte)(point.Color.r * 255.0f));
                    binWriter.Write((byte)(point.Color.g * 255.0f));
                    binWriter.Write((byte)(point.Color.b * 255.0f));
                }
            }
        }

        public static void Write2Bin(Dictionary<Vector3, List<CloudPoint>> points)
        {
            String dataPath = Path.Combine(Application.persistentDataPath, "Data");

            if (!Directory.Exists(dataPath))
            {
                Directory.CreateDirectory(dataPath);
            }

            int fileIdx = 0;
            foreach (var item in points)
            {
                String fileName = String.Format("part_{0}_.bin", fileIdx.ToString());
                String target = Path.Combine(dataPath, fileName);

                using (BinaryWriter writer = new BinaryWriter(File.Open(target, FileMode.OpenOrCreate)))
                {
                    for (int i = 0; i < item.Value.Count; i++)
                    {
                        Vector3 vec = item.Value[i].Vector;
                        writer.Write(vec.x);
                        writer.Write(vec.y);
                        writer.Write(vec.z);

                        Color clr = item.Value[i].Color;
                        writer.Write(clr.r);
                        writer.Write(clr.g);
                        writer.Write(clr.b);
                    }
                }

                fileIdx++;
            }
        }

        /// <summary>
        /// Call this when 1 row is 24 byte
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        public static List<CloudPoint> ReadBinary(String filePath, int skip = 1)
        {
            List<CloudPoint> points = new List<CloudPoint>();

            using (FileStream fs = File.Open(filePath, FileMode.Open))
            {
                FileInfo info = new FileInfo(filePath);
                int numPoints = (int)info.Length / 24;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    if (skip <= 1)
                    {
                        for (int i = 0; i < numPoints; i++)
                        {
                            float x = br.ReadSingle();
                            float y = br.ReadSingle();
                            float z = br.ReadSingle();

                            float r = br.ReadSingle();
                            float g = br.ReadSingle();
                            float b = br.ReadSingle();

                            Vector3 vec = new Vector3(x, y, z);
                            Color color = new Color(r, g, b);
                            points.Add(new CloudPoint(vec, color));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < numPoints / skip; i++)
                        {
                            int jump = skip - 1;
                            br.ReadBytes(jump * 24);

                            float x = br.ReadSingle();
                            float y = br.ReadSingle();
                            float z = br.ReadSingle();

                            float r = br.ReadSingle();
                            float g = br.ReadSingle();
                            float b = br.ReadSingle();

                            Vector3 vec = new Vector3(x, y, z);
                            Color color = new Color(r, g, b);
                            points.Add(new CloudPoint(vec, color));
                        }
                    }
                }
            }
            return points;
        }

        /// <summary>
        /// Call this when 1 row is 15 byte
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        public static List<CloudPoint> ReadBinary_15(String filePath, int skip = 1)
        {
            List<CloudPoint> points = new List<CloudPoint>();

            using (FileStream fs = File.Open(filePath, FileMode.Open))
            {
                FileInfo info = new FileInfo(filePath);
                int numPoints = (int)info.Length / 15;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    if (skip <= 1)
                    {
                        for (int i = 0; i < numPoints; i++)
                        {
                            float x = br.ReadSingle();
                            float y = br.ReadSingle();
                            float z = br.ReadSingle();

                            int r = (int)br.ReadByte();
                            int g = (int)br.ReadByte();
                            int b = (int)br.ReadByte();

                            Vector3 vec = new Vector3(x, y, z);
                            Color color = new Color(r / 255.0f, g / 255.0f, b / 255.0f);
                            points.Add(new CloudPoint(vec, color));
                        }
                    }
                    else
                    {
                        for (int i = 0; i < numPoints / skip; i++)
                        {
                            int jump = skip - 1;
                            br.ReadBytes(jump * 15);

                            float x = br.ReadSingle();
                            float y = br.ReadSingle();
                            float z = br.ReadSingle();
                            
                            int r = (int)br.ReadByte();
                            int g = (int)br.ReadByte();
                            int b = (int)br.ReadByte();
                            
                            Vector3 vec = new Vector3(x, y, z);
                            Color color = new Color(r / 255.0f, g / 255.0f, b / 255.0f);
                            points.Add(new CloudPoint(vec, color));
                        }
                    }
                }
            }
            return points;
        }

        /// <summary>
        /// Call this when 1 row is 19 byte
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        public static List<CloudPoint> ReadBinary_19(String filePath, float rate)
        {
            List<CloudPoint> points = new List<CloudPoint>();

            using (FileStream fs = File.Open(filePath, FileMode.Open))
            {
                FileInfo info = new FileInfo(filePath);
                int numPoints = (int)info.Length / 19;

                using (BinaryReader br = new BinaryReader(fs))
                {
                    if (rate == 1)
                    {
                        for (int i = 0; i < numPoints; i++)
                        {
                            float x = br.ReadSingle();
                            float y = br.ReadSingle();
                            float z = br.ReadSingle();

                            float intensity = br.ReadSingle();

                            int r = (int)br.ReadByte();
                            int g = (int)br.ReadByte();
                            int b = (int)br.ReadByte();

                            Vector3 vec = new Vector3(x, y, z);
                            Color color = new Color(r / 255.0f, g / 255.0f, b / 255.0f);
                            points.Add(new CloudPoint(vec, color, intensity));
                        }
                    }
                    else if (rate <= 0.1f)
                    {
                        for (int i = 0; i < numPoints / 10; i++)
                        {
                            br.ReadBytes(76);
                            ReadPoint(br, ref points);
                            br.ReadBytes(95);
                        }
                    }
                    else if (rate <= 0.2f)
                    {
                        for (int i = 0; i < numPoints / 5; i++)
                        {
                            br.ReadBytes(76);
                            ReadPoint(br, ref points);
                        }
                    }
                    else if (rate <= 0.3f)  //33%
                    {
                        for (int i = 0; i < numPoints / 3; i++)
                        {
                            br.ReadBytes(38);
                            ReadPoint(br, ref points);
                        }
                    }
                    else if (rate <= 0.4f)
                    {
                        for (int i = 0; i < numPoints; i++)
                        {
                            if (i % 2 == 0 && i % 10 != 0)
                            {
                                ReadPoint(br, ref points);
                            }
                            else
                            {
                                br.ReadBytes(19);
                            }
                        }
                    }
                    else if (rate <= 0.5f)
                    {
                        for (int i = 0; i < numPoints / 2; i++)
                        {
                            br.ReadBytes(19);
                            ReadPoint(br, ref points);
                        }
                    }
                    else if (rate == 0.6f)
                    {
                        for (int i = 0; i < numPoints / 10; i++)
                        {
                            ReadPoint(br, ref points);
                            ReadPoint(br, ref points);

                            br.ReadBytes(19);

                            ReadPoint(br, ref points);
                            ReadPoint(br, ref points);

                            br.ReadBytes(38);

                            ReadPoint(br, ref points);
                            ReadPoint(br, ref points);

                            br.ReadBytes(19);
                        }
                    }
                    else if (rate == 0.7f)
                    {
                        for (int i = 0; i < numPoints / 10; i++)
                        {
                            ReadPoint(br, ref points);
                            ReadPoint(br, ref points);
                            ReadPoint(br, ref points);

                            br.ReadBytes(38);

                            ReadPoint(br, ref points);
                            ReadPoint(br, ref points);
                            ReadPoint(br, ref points);

                            br.ReadBytes(19);

                            ReadPoint(br, ref points);
                        }
                    }
                    else if (rate == 0.8f)
                    {
                        int j = 0;

                        for (int i = 0; i < numPoints; i++)
                        {
                            if (j % 10 == 0)
                            {
                                j = 0;
                            }

                            if (j == 3 || j == 9)
                            {
                                br.ReadBytes(19);
                            }
                            else
                            {
                                ReadPoint(br, ref points);
                            }
                            j++;
                        }
                    }
                    else if (rate == 0.9f)
                    {
                        for (int i = 0; i < numPoints; i++)
                        {
                            if (i % 10 != 0 && i % 5 == 0)
                            {
                                br.ReadBytes(19);
                            }
                            else
                            {
                                ReadPoint(br, ref points);
                            }
                        }
                    }
                }
            }
            return points;
        }

        private static void ReadPoint(BinaryReader br, ref List<CloudPoint> points)
        {
            float x = br.ReadSingle();
            float y = br.ReadSingle();
            float z = br.ReadSingle();

            float intensity = br.ReadSingle();

            int r = (int)br.ReadByte();
            int g = (int)br.ReadByte();
            int b = (int)br.ReadByte();

            Vector3 vec = new Vector3(x, y, z);
            Color color = new Color(r / 255.0f, g / 255.0f, b / 255.0f);
            points.Add(new CloudPoint(vec, color, intensity));
        }
    }
}