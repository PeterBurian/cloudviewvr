﻿using UnityEngine.EventSystems;

public interface IJoyMode : IEventSystemHandler
{
    void Step2Next();
}