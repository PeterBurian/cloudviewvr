﻿using UnityEngine.EventSystems;

namespace CloudView.Controller
{
    public interface IMove : IEventSystemHandler
    {
        void PlayerMoved();
    }
}