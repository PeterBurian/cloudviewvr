﻿using UnityEngine.EventSystems;

namespace CloudView.Controller
{
    public interface IControllPressed : IEventSystemHandler
    {
        void A_Pressed();

        void B_Pressed();

        void C_Pressed();

        void D_Pressed();
    }

    public interface IControllReleased : IEventSystemHandler
    {
        void A_Released();

        void B_Released();

        void C_Released();

        void D_Released();
    }
}