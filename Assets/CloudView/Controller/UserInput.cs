﻿//#define DEBUG_MODE

using CloudView.Controller;
using CloudView.Events;
using System;
using UnityEngine;

namespace CloudView.Controller
{
    public class UserInput : MonoBehaviour
    {
        /*
         VR_LIONEYE HAVE 4 MOD; 
              FOR MOD CHANGE PRESS @-A   -Mouse mod ** my codes not work on this mode
                                   @-B   -Horizontal bluetooth controller mode
                                   @-C   -Vertical bluetooth controller mode
                                   @-D   -Inactive mod also don't work nothing
             for activate or changing modes press @ and other button what you need same time until see light

        joystick button 3           - A     -X1
        joystick button 0           - B     -X2
        joystick button 2           - C
        joystick button 1           - D
        joystick button 11          - OnOff //only work @C mod 

        */

        private static KeyCode lastPressedKey;
        private static bool isKeyPressed;
        private static long lastPressedTicks;

        public void Awake()
        {
            isKeyPressed = false;
            lastPressedTicks = 0;
        }

        public void Update()
        {
            float roll = Input.GetAxis("Horizontal");     //joystick horizontal
            float pitch = Input.GetAxis("Vertical");      //joystick vertical

            Vector3 diffPos = new Vector3(roll * 0.1f, pitch * 0.1f, 0);

            //Bluetooth Controller Joystick
            CheckAxis();

            //Bluetooth Controller Buttons
            CheckButtons();


#if UNITY_EDITOR

            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                lastPressedKey = KeyCode.LeftShift;
                isKeyPressed = true;
            }
            else if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                lastPressedKey = KeyCode.LeftControl;
                isKeyPressed = true;
            }
            else if (Input.GetKeyDown(KeyCode.RightControl))
            {
                lastPressedKey = KeyCode.RightControl;
                isKeyPressed = true;
            }
            else if (Input.GetKeyDown(KeyCode.RightShift))
            {
                lastPressedKey = KeyCode.RightShift;
                isKeyPressed = true;
            }

            CallPressedButton();
#endif
        }

        private bool IsNotRepeating()
        {
            long diff = DateTime.Now.Ticks - lastPressedTicks;
            return lastPressedTicks == 0 || diff > TimeSpan.TicksPerSecond;
        }

        private void ButtonName(string buttonName)
        {
            //Debug.Log(buttonName);
        }

        private void CheckAxis()
        {
            if (Input.GetAxis("Vertical") > 0)
            {
                ButtonName("Up Pressed");
            }
            if (Input.GetAxis("Vertical") < 0)
            {
                ButtonName("Down Pressed");
            }
            if (Input.GetAxis("Horizontal") > 0)
            {
                ButtonName("Right Pressed");
            }
            if (Input.GetAxis("Horizontal") < 0)
            {
                ButtonName("Left Pressed");
            }
        }

        private void CheckButtons()
        {
#if UNITY_EDITOR
            if (Input.GetKeyDown(KeyCode.Q))
            {
                lastPressedKey = KeyCode.JoystickButton0;
                isKeyPressed = true;
            }
            
#else
            if (Input.GetKey(KeyCode.JoystickButton0))
            {
                lastPressedKey = KeyCode.JoystickButton0;
                isKeyPressed = true;
            }
            else if (Input.GetKey(KeyCode.JoystickButton1))
            {
                lastPressedKey = KeyCode.JoystickButton1;
                isKeyPressed = true;
            }
            else if (Input.GetKey(KeyCode.JoystickButton2))
            {
                lastPressedKey = KeyCode.JoystickButton2;
                isKeyPressed = true;
            }
            else if (Input.GetKey(KeyCode.JoystickButton3))
            {
                lastPressedKey = KeyCode.JoystickButton3;
                isKeyPressed = true;
            }
            else if (Input.GetKey(KeyCode.JoystickButton4))
            {
                ButtonName("Press J_4");
            }
            else if (Input.GetKey(KeyCode.JoystickButton5))
            {
                ButtonName("Press J_5");
            }
#endif

            CallPressedButton();
        }

        private void CheckKeyUp()
        {
            if (Input.GetKeyUp(KeyCode.JoystickButton0))
            {
                EventBus.Instance.Post<IControllReleased>((e, f) => e.B_Released());
                ButtonName("B Released");
            }
            if (Input.GetKeyUp(KeyCode.JoystickButton1))
            {
                EventBus.Instance.Post<IControllReleased>((e, f) => e.D_Released());
                ButtonName("D Released");
            }
            if (Input.GetKeyUp(KeyCode.JoystickButton2))
            {
                EventBus.Instance.Post<IControllReleased>((e, f) => e.C_Released());
                ButtonName("C Released");
            }
            if (Input.GetKeyUp(KeyCode.JoystickButton3))
            {
                EventBus.Instance.Post<IControllReleased>((e, f) => e.A_Released());
                ButtonName("A Released");
            }
            if (Input.GetKeyUp(KeyCode.JoystickButton4))
            {
                ButtonName("Released J_4");
            }
            if (Input.GetKeyUp(KeyCode.JoystickButton5))
            {
                ButtonName("Released J_5");
            }
        }

        private void CheckButtons_VR_Lioneye()
        {
            // Bluetooth Controller Buttons VR_Lioneye
            if (Input.GetButtonDown("A"))
            {
                ButtonName("A");
            }
            if (Input.GetButtonDown("B"))
            {
                ButtonName("B");
            }
            if (Input.GetButtonDown("C"))
            {
                ButtonName("C");
            }
            if (Input.GetButtonDown("D"))
            {
                ButtonName("D");
            }

            if (Input.GetButtonDown("OnOff"))
            {
                ButtonName("OnOff");
            }
        }

        private void CallPressedButton()
        {
            if (isKeyPressed && IsNotRepeating())
            {
                isKeyPressed = false;
                lastPressedTicks = DateTime.Now.Ticks;
                CallButton(lastPressedKey);
            }
        }

        private void CallButton(KeyCode code)
        {
            switch (code)
            {
                case KeyCode.JoystickButton3:
                case KeyCode.LeftShift:
                    EventBus.Instance.Post<IControllPressed>((e, f) => e.A_Pressed());
                    break;

                case KeyCode.JoystickButton2:
                case KeyCode.RightShift:
                    EventBus.Instance.Post<IControllPressed>((e, f) => e.C_Pressed());
                    break;

                case KeyCode.JoystickButton0:
                case KeyCode.LeftControl:
                    EventBus.Instance.Post<IControllPressed>((e, f) => e.B_Pressed());
                    break;

                case KeyCode.JoystickButton1:
                case KeyCode.RightControl:
                    EventBus.Instance.Post<IControllPressed>((e, f) => e.D_Pressed());
                    break;
            }
        }
    }
}