﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class ChangeScene : MonoBehaviour
    {
        public void ChangeToScene(int ChangeTheScene)
        {
            SceneManager.LoadScene(ChangeTheScene);
        }
    }
}