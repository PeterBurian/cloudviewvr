﻿using System;
using System.IO;
using System.Text;
using UnityEngine;

namespace Utilities
{
    public class Log
    {
        private String path;
        private String separator;

        public void Print(Exception e)
        {
            Print(e.ToString());
        }

        public void Print(Exception e, bool enableUnityLog)
        {
            String dir = Path.GetDirectoryName(path);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(path);
            }
            Print(e.ToString(), enableUnityLog);
        }

        public void Print(String message, bool enableUnityLog = false)
        {
            message += separator;

            try
            {
                using (StreamWriter file = new StreamWriter(path, true))
                {
                    file.WriteLine(message);

                    if (enableUnityLog)
                    {
                        Debug.Log(message);
                    } 
                }
            }
            catch (IOException e)
            {
                Debug.LogError("IO Exception: " + e.ToString());
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }
        }

        private String SetPath()
        {
            String path = String.Empty;
            String fileName = String.Format("{0:yyyy-MM-dd}.log", DateTime.Now);

#if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_STANDALONE_LINUX
            path = Application.dataPath + "/StreamingAssets/Log";
#elif UNITY_ANDROID
                path = Application.persistentDataPath + "/Log";
#elif UNITY_IOS
				path = Application.persistentDataPath + "/Log";
#endif

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path + "/" + fileName;
        }

        private String SetSeparator()
        {
            StringBuilder separator = new StringBuilder(Environment.NewLine);
            separator.Append("*-*-*-*-#-#-XXX-#-#-*-*-*-*");
            separator.Append(Environment.NewLine);

            return separator.ToString();
        }

        //----------Singleton part--------//
        #region Singleton

        private static Log log;

        public static Log Logger
        {
            get
            {
                if (log == null)
                {
                    log = new Log();
                }
                return log;
            }
        }

        private Log()
        {
            path = SetPath();
            separator = SetSeparator();
        }

        #endregion
    }
}