﻿using System;

namespace Utilities
{
    public static class Strings
    {
        public static readonly String Resources = "Resources";
        public static readonly String PointCloudMeshes = "PointCloudMeshes";
        public static readonly String Flags = "Flags";
    }
}