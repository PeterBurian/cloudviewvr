﻿using Models;
using Models.Ptx;
using UnityEngine;

namespace Utilities.Extensions
{
    public static class CloudPointExtension
    {
        public static CloudPoint Convert(ScanHeader header, CloudPoint pt)
        {
            float x = header.pos.x + pt.Vector.x * header.axisX.x + pt.Vector.y * header.axisY.x + pt.Vector.z * header.axisZ.x;
            float y = header.pos.y + pt.Vector.x * header.axisX.y + pt.Vector.y * header.axisY.y + pt.Vector.z * header.axisZ.y;
            float z = header.pos.z + pt.Vector.x * header.axisX.z + pt.Vector.y * header.axisY.z + pt.Vector.z * header.axisZ.z;

            pt.Vector = new Vector3(x, y, z);
            return pt;
        }
    }
}