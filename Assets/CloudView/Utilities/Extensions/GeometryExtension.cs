﻿using System.Collections.Generic;
using UnityEngine;

namespace Utilities.Extensions
{
    public static class GeometryExtension
    {
        public static bool PointInPolygon(IList<Vector2> polygon, Vector2 checkpoint)
        {
            int k, j = polygon.Count - 1;
            bool oddNodes = false;
            for (k = 0; k < polygon.Count; k++)
            {
                Vector2 polyK = polygon[k];
                Vector2 polyJ = polygon[j];

                if (((polyK.y > checkpoint.y) != (polyJ.y > checkpoint.y))
                && (checkpoint.x < (polyJ.x - polyK.x) * (checkpoint.y - polyK.y) / (polyJ.y - polyK.y) + polyK.x))
                    oddNodes = !oddNodes;
                j = k;
            }
            return oddNodes;
        }

        public static bool LineSegmentIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4)
        {
            Vector2 a = p2 - p1;
            Vector2 b = p3 - p4;
            Vector2 c = p1 - p3;

            float alphaNumerator = b.y * c.x - b.x * c.y;
            float alphaDenominator = a.y * b.x - a.x * b.y;
            float betaNumerator = a.x * c.y - a.y * c.x;
            float betaDenominator = alphaDenominator;

            bool doIntersect = true;

            if (alphaDenominator == 0 || betaDenominator == 0)
            {
                doIntersect = false;
            }
            else
            {

                if (alphaDenominator > 0)
                {
                    if (alphaNumerator < 0 || alphaNumerator > alphaDenominator) doIntersect = false;
                }
                else if (alphaNumerator > 0 || alphaNumerator < alphaDenominator)
                {
                    doIntersect = false;
                }

                if (doIntersect && betaDenominator > 0)
                {
                    if (betaNumerator < 0 || betaNumerator > betaDenominator) doIntersect = false;
                }
                else if (betaNumerator > 0 || betaNumerator < betaDenominator)
                {
                    doIntersect = false;
                }
            }
            return doIntersect;
        }

        public static bool ConvexPolygonOverlap(Vector2[] aVertList, Vector2[] bVertList)
        {

            // First, use all of A's edges to get candidate separating axes
            if (FindSeparatingAxis(aVertList, bVertList))
            {
                return false;
            }

            // Now swap roles, and use B's edges
            if (FindSeparatingAxis(bVertList, aVertList))
            {
                return false;
            }

            // No separating axis found.  They must overlap
            return true;
        }

        private static bool FindSeparatingAxis(Vector2[] aVertList, Vector2[] bVertList)
        {
            // Iterate over all the edges
            int prev = 3;

            for (int cur = 0; cur < 4; ++cur)
            {
                // Get edge vector.  (Assume operator- is overloaded)
                Vector2 edge = aVertList[cur] - aVertList[prev];

                // Rotate vector 90 degrees (doesn't matter which way) to get
                // candidate separating axis.
                Vector2 v;
                v.x = edge.y;
                v.y = -edge.x;

                // Gather extents of both polygons projected onto this axis
                float aMin = 0;
                float aMax = 0;
                float bMin = 0;
                float bMax = 0;

                GatherPolygonProjectionExtents(aVertList, ref v, ref aMin, ref aMax);
                GatherPolygonProjectionExtents(bVertList, ref v, ref bMin, ref bMax);

                // Is this a separating axis?
                if (aMax < bMin) return true;
                if (bMax < aMin) return true;

                // Next edge, please
                prev = cur;
            }

            // Failed to find a separating axis
            return false;
        }

        private static void GatherPolygonProjectionExtents(Vector2[] vertList, ref Vector2 v, ref float outMin, ref float outMax)
        {
            // Initialize extents to a single point, the first vertex
            outMin = outMax = Dot(v, vertList[0]);

            // Now scan all the rest, growing extents to include them
            for (int i = 1; i < 4; ++i)
            {
                float d = Dot(v, vertList[i]);
                if (d < outMin) outMin = d;
                else if (d > outMax) outMax = d;
            }
        }

        private static float Dot(Vector2 a, Vector2 b)
        {
            return a.x* b.x + a.y* b.y;
        }
    }
}
