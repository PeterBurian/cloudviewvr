﻿using Database;
using Database.Model;
using System;
using System.IO;
using UnityEngine;

namespace Utilities
{
    public static class Paths
    {
        private static String projectName = "";
        public static String dbName = "cview.sqlite";
        public static String confName = "project.conf";
        public static String dataExtension = ".bin";

        //Some Unity paths for threads
        public static String streamingAssetsPath;
        public static String persistentDataPath;

        public static void SetUnityPaths()
        {
            streamingAssetsPath = Application.streamingAssetsPath;
            persistentDataPath = Application.persistentDataPath;
        }

        public static String Resources()
        {
            return Application.dataPath + "/Resources/";
        }

        public static String PointCloudMeshes()
        {
            return Application.dataPath + "/Resources/PointCloudMeshes/";
        }

        public static String StreamingData()
        {
            return Path.Combine(Application.streamingAssetsPath, "Data");

        }

        public static String Data()
        {
            return Path.Combine(Application.persistentDataPath, "Data");
        }

        public static String PersistentFolder()
        {
            String folder = Path.Combine(Application.persistentDataPath, "Meshes");

            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            return folder;
        }

        public static String DbPath()
        {
            return Path.Combine(Application.persistentDataPath, dbName);
        }

        public static String ConfPath()
        {
            return Path.Combine(Application.persistentDataPath, dbName);
        }

        public static String ProjectName()
        {
            if (String.IsNullOrEmpty(projectName))
            {
                Project project = SqliteAdapter.SelectFirst<Project>();

                if (project != null)
                {
                    projectName = project.name;
                }
            }
            return projectName;
        }
    }
}
