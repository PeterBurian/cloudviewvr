﻿using Assets.CloudView;
using Database;
using Database.Model;
using Handler;
using Models;
using MovementEffects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Utilities
{
    //This is not a real builder pattern

    public class MeshBuilder
    {
        private static Material matVertex;
        private static int colorMode;
        private GameObject pointCloud; //this is the parent of the meshes
        
        public MeshBuilder(Material material)
        {
            matVertex = material;
            pointCloud = GameObject.Find("Meshes");

            colorMode = -1;
            ChangeColorMode();

            IEnumerator<float> horMthd = SetHorizontal();
            Timing.RunCoroutine(horMthd);
        }

        public KeyValuePair<String, List<CloudPoint>> ReadPoints(String meshPath, String id, float rate)
        {
            List<CloudPoint> points = BinaryHandler.ReadBinary_19(meshPath, rate);
            return new KeyValuePair<String, List<CloudPoint>>(id, points);
        }

        public void ReadPoints(String meshPath, float rate, bool deleteFormer)
        {
            List<CloudPoint> points = BinaryHandler.ReadBinary_19(meshPath, rate);

            String meshName = Path.GetFileNameWithoutExtension(meshPath);
            CreateMeshGameobject(meshName, points, deleteFormer);
        }

        public void ReadPoints(List<CloudPoint> points, String meshPath, bool deleteFormer = false)
        {
            String meshName = Path.GetFileNameWithoutExtension(meshPath);
            CreateMeshGameobject(meshName, points, deleteFormer);
        }

        private GameObject CreateMeshGameobject(String meshName, List<CloudPoint> points, bool deleteFormer = false)
        {
            if (deleteFormer)
            {
                GameObject go = GameObject.Find(meshName);
                if (go != null)
                {
                    GameObject.Destroy(go);
                }
            }

            GameObject pointGroup = new GameObject(meshName);
            pointGroup.AddComponent<MeshFilter>();
            pointGroup.AddComponent<MeshRenderer>();
            pointGroup.GetComponent<Renderer>().material = matVertex;
            pointGroup.GetComponent<MeshFilter>().mesh = BuildMesh(points);
            pointGroup.layer = 15;
            pointGroup.transform.parent = pointCloud.transform;
            pointGroup.transform.localPosition = Vector3.zero;

            BoxCollider collider = pointGroup.AddComponent<BoxCollider>();
            collider.isTrigger = true;

            return pointGroup;
        }

        private Mesh BuildMesh(List<CloudPoint> points)
        {
            Mesh mesh = new Mesh();
            int nPoints = points.Count;

            Vector3[] meshVerts = new Vector3[nPoints];
            int[] indecies = new int[nPoints];
            Color[] myColors = new Color[nPoints];

            for (int i = 0; i < nPoints; ++i)
            {
                Color color = points[i].Color;
                meshVerts[i] = points[i].Vector;
                indecies[i] = i;
                myColors[i] = new Color(color.r, color.g, color.b, points[i].Intensity);
            }

            mesh.vertices = meshVerts;

            mesh.colors = myColors;
            mesh.SetIndices(indecies, MeshTopology.Points, 0);
            mesh.uv = new Vector2[nPoints];

            mesh.normals = new Vector3[nPoints];

            mesh.RecalculateBounds();

            return mesh;
        }

        private IEnumerator<float> SetHorizontal()
        {
            List<PtxGroupHeader> headers = SqliteAdapter.SelectAll<PtxGroupHeader>();

            if (headers != null && headers.Count > 0)
            {
                PtxGroupHeader first = headers[0];

                Vector3 min = new Vector3(first.Min_X, first.Min_Y, first.Min_Z);

                for (int i = 1; i < headers.Count; i++)
                {
                    PtxGroupHeader header = headers[i];

                    min.y = header.Min_Y < min.y ? header.Min_Y : min.y;
                    min.z = header.Min_Z < min.z ? header.Min_Z : min.z;
                }

                //Have to solve this problem, in header y and z not replaced
                Configs.verticalPush = -min.z;
                //pointCloud.transform.position = new Vector3(-110.0f, Configs.verticalPush, -100);
                pointCloud.transform.position = new Vector3(-110.0f, 15, -100);
            }
            

            yield return Timing.WaitForOneFrame;
        }

        public static void ChangeColorMode()
        {
            matVertex.SetInt("_RealColor", ++colorMode % 3);
        }
    }
}