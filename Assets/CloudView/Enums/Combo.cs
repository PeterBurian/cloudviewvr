﻿namespace CloudView.Enums
{
    public enum Combo
    {
        MOVE,
        TELEPORT,
        LIFT,
        MEASURE,
        SHADERS
    }
}