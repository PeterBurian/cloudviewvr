﻿namespace Scripts.Enums
{
    public enum Density
    {
        FULL = 1,
        HIGH = 2,
        MEDIUM = 4,
        LOW = 6,
        NONE
    }
}