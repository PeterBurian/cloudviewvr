﻿using MovementEffects;
using Scripts.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Assets.CloudView
{
    public partial class Loader
    {
        private void LoadMeshesWithDensity(Dictionary<Density, List<String>> groups)
        {
            object[] p = new object[2 * 4];
            int i = 0;
            int count = 0;

            foreach (var item in groups)
            {
                p[i] = item.Value;
                p[i + 1] = (int)item.Key;
                i = i + 2;

                count += item.Value.Count;
            }

            IEnumerator coroutine = _LoadMeshesWithDensity(p, count);
            StartCoroutine(coroutine);
        }

        //TEST
        private IEnumerator<float> _LoadMeshesWithDensity(object[] parameters, int numPoints)
        {
            int counter = 0;
            for (int j = 0; j < 8; j += 2)
            {
                List<String> parts = (List<String>)parameters[j];
                int density = (int)parameters[j + 1];

                for (int i = 0; i < parts.Count; i++)
                {
                    if (cached.ContainsKey(parts[i]))
                    {
                        builder.ReadPoints(cached[parts[i]], parts[i]);
                    }
                    else
                    {
                        builder.ReadPoints(parts[i], density, false);
                    }

                    counter += 1;
                    progress = counter * 1.0f / (numPoints - 1) * 1.0f;

                    int devide = Mathf.FloorToInt(numPoints / 15);

                    if (devide != 0 && counter % devide == 0)
                    {
                        float percent = (counter / (float)numPoints) * 100.0f;
                        guiText = percent + "%";
                        yield return Timing.WaitForOneFrame;
                    }
                }
            }

            if (watch != null)
            {
                watch.StopWatch();
            }

            showGui = false;
            yield return Timing.WaitForOneFrame;
        }

        private String[] GetAllfiles(String datafolderPath)
        {
            return Directory.GetFiles(datafolderPath, "*.bin");
        }
    }
}
