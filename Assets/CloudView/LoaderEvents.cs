﻿using CloudView.Controller;
using CloudView.Events;

namespace Assets.CloudView
{
    public partial class Loader : IControllPressed, ILoader
    {
        public void A_Pressed() { }

        public void B_Pressed() { }

        public void C_Pressed() { }

        public void D_Pressed()
        {
            MovedByFPSC();
        }

        public void MovedByFPSC()
        {
            EventBus.Instance.Post<IMove>((e, f) => e.PlayerMoved());
            Reload();
        }
    }
}
