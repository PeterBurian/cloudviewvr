﻿using CloudView.Events;
using UnityEngine;

namespace Assets.CloudView.Sensors
{
    public class Pedometer : MonoBehaviour
    {
        public float loLim = 0.005f;    // level to fall to the low state 
        public float hiLim = 0.1f;      // level to go to high state (and detect step) 
        public int steps = 0;           // step counter - counts when comp state goes high private 
        bool stateH = false;            // comparator state

        public float fHigh = 10.0f;     // noise filter control - reduces frequencies above fHigh private 
        public float curAcc = 0f;       // noise filter 
        public float fLow = 0.1f;       // average gravity filter control - time constant about 1/fLow 
        float avgAcc = 0f;

        public int wait_time = 30;
        //private int old_steps;
        //private int counter = 30;

        public bool isStep;

        private void Awake()
        {
            isStep = false;

            avgAcc = Input.acceleration.magnitude; // initialize avg filter
            //old_steps = steps;
        }

        #region Update
        //private void Update()
        //{
        //    if (counter > 0)
        //    {
        //        counter--;
        //        return;
        //    }

        //    counter = wait_time;

        //    if (steps != old_steps)
        //    {
        //        WALKING
        //    }

        //    old_steps = steps;
        //}

        #endregion

        private void FixedUpdate()
        { 
            // filter input.acceleration using Lerp
            curAcc = Mathf.Lerp(curAcc, Input.acceleration.magnitude, Time.deltaTime * fHigh);
            avgAcc = Mathf.Lerp(avgAcc, Input.acceleration.magnitude, Time.deltaTime * fLow);
            float delta = curAcc - avgAcc; // gets the acceleration pulses

            if (isStep)
            {
                stateH = false;
            }

            if (!stateH)
            { 
                // if state == low...
                if (delta > hiLim || isStep)
                {
                    isStep = false;
                    // only goes high if input > hiLim
                    stateH = true;
                    steps++; // count step when comp goes high
                    //fpsGo.IsStepTaken();

                    EventBus.Instance.Post<IPedometer>((e, f) => e.StepTaken());
                }
            }
            else
            {
                if (delta < loLim)
                { 
                    // only goes low if input < loLim 
                    stateH = false;
                }
            }
        }
    }
}
