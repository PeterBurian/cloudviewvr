﻿using UnityEngine.EventSystems;

namespace Assets.CloudView.Sensors
{
    public interface IPedometer : IEventSystemHandler
    {
        void StepTaken();
    }
}