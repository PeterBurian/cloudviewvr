﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Assets.CloudView.Sensors
{
    interface IMarch : IEventSystemHandler
    {
        void Lift(bool isUp);

        void TeleportNext(Vector3 position);
    }
}