﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace CloudView.Events
{
    public class EventBus : ScriptableObject
    {
        private static EventBus instance;

        public static EventBus Instance
        {
            get
            {
                return Init();
            }
        }

        private readonly List<KeyValuePair<GameObject, Type>> events = new List<KeyValuePair<GameObject, Type>>();

        public static EventBus Init()
        {
            if (instance == null)
            {
                instance = ScriptableObject.CreateInstance<EventBus>();
            }
            return instance;
        }

        public void Register<T>(GameObject obj)
        {
            events.Add(new KeyValuePair<GameObject, Type>(obj, typeof(T)));
        }

        public void Unregister(GameObject obj)
        {
            events.RemoveAll(e => e.Key == obj);
        }

        public void Post<T>(ExecuteEvents.EventFunction<T> func) where T : IEventSystemHandler
        {
            GameObject[] objectsToFire = events.Where(a => a.Value == typeof(T)).Select(a => a.Key).ToArray();

            foreach (GameObject go in objectsToFire)
            {
                try
                {
                    if (go != null)
                    {
                        ExecuteEvents.Execute<T>(go, null, func);
                    }
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex.ToString());
                }
            }
        }

        public static void PostChildren<T>(GameObject root, ExecuteEvents.EventFunction<T> mthd) where T : IEventSystemHandler
        {
            MonoBehaviour[] monos = root.GetComponentsInChildren<MonoBehaviour>().Where(a => a is T).ToArray();

            foreach (MonoBehaviour child in monos)
            {
                try
                {
                    if (child.gameObject != null) ExecuteEvents.Execute<T>(child.gameObject, null, mthd);
                }
                catch (Exception ex)
                {
                    Debug.LogError(ex.ToString());
                }
            }
        }

        public static void PostChildren<T>(MonoBehaviour root, ExecuteEvents.EventFunction<T> mthd) where T : IEventSystemHandler
        {
            try
            {
                if (root.gameObject != null)
                {
                    PostChildren<T>(root.gameObject, mthd);
                } 
            }
            catch (Exception exception)
            {
                Debug.LogError(exception.ToString());
            }
        }

        public static void PostParent<T>(GameObject root, ExecuteEvents.EventFunction<T> mthd) where T : IEventSystemHandler
        {
            try
            {
                if (root.gameObject != null)
                {
                    ExecuteEvents.ExecuteHierarchy<T>(root, null, mthd);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.ToString());
            }
        }

        public static void PostParent<T>(MonoBehaviour root, ExecuteEvents.EventFunction<T> mthd) where T : IEventSystemHandler
        {
            try
            {
                if (root.gameObject != null)
                {
                    ExecuteEvents.ExecuteHierarchy<T>(root.gameObject, null, mthd);
                }
            }
            catch (Exception ex)
            {
                Debug.LogError(ex.ToString());
            }
        }
    }
}