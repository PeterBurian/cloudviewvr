﻿using UnityEngine;

namespace CloudView.Settings
{
    public class UserSettings
    {
        public int GetDensity()
        {
            return PlayerPrefs.GetInt(PlayerKeys.Density, 50);
        }

        public void SetDensity(int density)
        {
            if (density > 0 && density <= 100)
            {
                PlayerPrefs.SetInt(PlayerKeys.Density, density);
            }
        }

        public int GetCurrentMode()
        {
            return PlayerPrefs.GetInt(PlayerKeys.Mode, 0);
        }

        public void Change2NextMode()
        {
            int currentMode = GetCurrentMode();
            SetCurrentMode(++currentMode);
        }

        public void Change2PrevMode()
        {
            int currentMode = GetCurrentMode();
            SetCurrentMode(--currentMode);
        }

        public void SetCurrentMode(int mode)
        {
            int modeCount = System.Enum.GetNames(typeof(Enums.Combo)).Length;

            int currentMode = 0;

            if (mode < 0)
            {
                currentMode = (mode + modeCount) % modeCount;
            }
            else
            {
                currentMode = mode % modeCount;
            }

            PlayerPrefs.SetInt(PlayerKeys.Mode, currentMode);
        }
    }
}