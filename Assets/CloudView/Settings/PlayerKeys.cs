﻿using System;

namespace CloudView.Settings
{
    public class PlayerKeys
    {
        public static readonly String Density = "Density";
        public static readonly String Mode = "CMode";
    }
}