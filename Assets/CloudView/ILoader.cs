﻿using UnityEngine.EventSystems;

namespace Assets.CloudView
{
    public interface ILoader : IEventSystemHandler
    {
        void MovedByFPSC();
    }
}
