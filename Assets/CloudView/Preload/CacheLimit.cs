﻿using UnityEngine;

namespace Assets.CloudView.Preload
{
    public enum PreLoad
    {
        From_10 = 10,
        From_50 = 50,
        From_100 = 100,
        From_Half,
        From_Custom
    }

    public class CacheLimit
    {
        private int count;
        private PreLoad threshold;
        private float percent;

        public CacheLimit(int count)
        {
            this.count = count;
            this.threshold = PreLoad.From_Half;
        }

        public CacheLimit(int count, PreLoad threshold)
        {
            this.count = count;
            this.threshold = threshold;
        }

        public CacheLimit(int count, float percent)
        {
            this.count = count;
            this.threshold = PreLoad.From_Custom;

            if (percent > 1)
            {
                percent /= 100;
            }
            this.percent = percent;
        }

        public int GetFrom4Preload()
        {
            int from = 50;

            if (threshold == PreLoad.From_10)
            {
                from = 10;
            }
            else if (threshold == PreLoad.From_50)
            {
                from = 50;
            }
            else if (threshold == PreLoad.From_100)
            {
                from = 100;
            }
            else if (threshold == PreLoad.From_Half)
            {
                from = count / 2;
            }
            else if (threshold == PreLoad.From_Custom)
            {
                Mathf.FloorToInt(percent * count);
            }
            else
            {
                from = count / 2;
            }

            return from;
        }
    }
}
