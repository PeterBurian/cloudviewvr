﻿using Database.Model;
using Models.Ptx;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Database
{
    public static class DbModelHandler
    {
        public static Vector CreateVector(Vector3 dat)
        {
            Vector vec = new Vector();
            vec.x = dat.x;
            vec.y = dat.y;
            vec.z = dat.z;
            vec.id = Guid.NewGuid().ToString();

            return vec;
        }

        public static void AddVector2List(bool isInvert, Vector3 dat, ref List<Vector> vectors)
        {
            Vector vec = new Vector();
            vec.x = dat.x;
            if (isInvert)
            {
                vec.y = dat.z;
                vec.z = dat.y;
            }
            else
            {
                vec.y = dat.y;
                vec.z = dat.z;
            }
            
            vec.id = Guid.NewGuid().ToString();

            vectors.Add(vec);
        }

        public static void SetDb_ProjectName(String prName)
        {
            Project project = new Project();
            project.id = Guid.NewGuid().ToString();
            project.name = prName;

            SqliteAdapter.Insert<Project>(project);
        }

        public static void AddItems2Db(Mesh mesh, String meshName)
        {
            AddItems2Db(mesh, meshName, "");
        }

        public static void AddItems2Db(Vector3 min, Vector3 max, String meshName, String camPosId, bool isInvert)
        {
            Vector minV = CreateVector(min);
            Vector maxV = CreateVector(max);
            Vector center = CreateVector((min + max) / 2);

            if (isInvert)
            {
                float tmp = min.y;
                minV.y = min.z;
                minV.z = tmp;
            }

            AddItems2Db(minV, maxV, center, meshName, camPosId);
        }

        public static void AddItems2Db(Mesh mesh, String meshName, String camPosId)
        {
            Vector minV = CreateVector(mesh.bounds.min);
            Vector maxV = CreateVector(mesh.bounds.max);
            Vector center = CreateVector(mesh.bounds.center);

            AddItems2Db(minV, maxV, center, meshName, camPosId);
        }

        public static void AddItems2Db(Vector minV, Vector maxV, Vector center, String meshName, String camPosId)
        {
            SqliteAdapter.Insert<Vector>(minV);
            SqliteAdapter.Insert<Vector>(maxV);
            SqliteAdapter.Insert<Vector>(center);

            MeshItem meshItem = new MeshItem();
            meshItem.FileName = meshName;
            meshItem.Min = minV.id;
            meshItem.Max = maxV.id;
            meshItem.Center = center.id;
            meshItem.GroupId = camPosId;
            meshItem.id = Guid.NewGuid().ToString();

            SqliteAdapter.Insert<MeshItem>(meshItem);
        }

        public static void AddHeader2Db(ScanGroup group, bool isInvert)
        {
            ScanHeader header = group.Header;
            PtxGroupHeader gHeader = new PtxGroupHeader();

            gHeader.col = header.col;
            gHeader.row = header.row;
            gHeader.GroupId = group.GetShortGuid();

            List<Vector> vectors = new List<Vector>();

            AddVector2List(isInvert, header.axisX, ref vectors);
            AddVector2List(isInvert, header.axisY, ref vectors);
            AddVector2List(isInvert, header.axisZ, ref vectors);
            AddVector2List(isInvert, header.pos, ref vectors);
            AddVector2List(isInvert, header.transformCoor, ref vectors);
            AddVector2List(isInvert, header.transformMatrix, ref vectors);
            AddVector2List(isInvert, header.rotation, ref vectors);
            AddVector2List(isInvert, header.precision, ref vectors);

            AddVector2List(isInvert, group.min, ref vectors);
            AddVector2List(isInvert, group.max, ref vectors);

            SqliteAdapter.InsertAll(vectors);

            gHeader.AxisX = vectors[0].id;
            gHeader.AxisY = vectors[1].id;
            gHeader.AxisZ = vectors[2].id;
            gHeader.Position = vectors[3].id;
            gHeader.TransformCoor = vectors[4].id;
            gHeader.TransformMatrix = vectors[5].id;
            gHeader.Rotation = vectors[6].id;
            gHeader.Precision = vectors[7].id;

            Vector min = vectors[8];
            gHeader.Min_X = min.x;
            gHeader.Min_Y = isInvert ? min.z : min.y;
            gHeader.Min_Z = isInvert ? min.y : min.z;

            Vector max = vectors[9];
            gHeader.Max_X = max.x;
            gHeader.Max_Y = isInvert ? max.z : max.y;
            gHeader.Max_Z = isInvert ? max.y : max.z;

            gHeader.id = Guid.NewGuid().ToString();

            SqliteAdapter.Insert<PtxGroupHeader>(gHeader);
        }

        public static void CreateDatabase()
        {
            String dbPath = Path.Combine(Application.streamingAssetsPath, "cview.sqlite");

            if (File.Exists(dbPath)) File.Delete(dbPath);
            File.Create(dbPath).Close();

            SqliteAdapter.Init(dbPath, true);

            if (SqliteAdapter.IsDbEmpty())
            {
                SqliteAdapter.CreateTable<Vector>();
                SqliteAdapter.CreateTable<MeshItem>();
                SqliteAdapter.CreateTable<Project>();
                SqliteAdapter.CreateTable<PtxGroupHeader>();
            }
        }
    }
}
