﻿using Database.Model;
using SimpleSQL;
using System;
using System.Collections.Generic;
using System.Linq;
using Utilities;

namespace Database
{
    public static class SqliteAdapter
	{
        private static SQLiteConnection _db;

        public static void Init(String dbPath, bool forceInit)
        {
            try
            {
                if (forceInit)
                {
                    CloseDB();
                }
                _db = new SQLiteConnection(dbPath);
            }
            catch (Exception ex)
            {
                Log.Logger.Print(ex, true);
            }
        }

        public static void CloseDB()
        {
            try
            {
                Dispose();
            }
            catch (Exception ex)
            {
                Log.Logger.Print(ex, true);
            }
        }

        public static void Insert<T>(T model) where T : Elem
		{
			try
			{
                if (_db != null)
                {
                    long rowID;
                    int ret = _db.Insert(model, out rowID);
                    if (ret <= 0)
                    {
                        Log.Logger.Print("SQl insert failed", true);
                    }
                }
			}
			catch (Exception ex)
			{
                Log.Logger.Print(ex, true);
            }
		}

        public static void InsertAll<T>(List<T> models) where T : Elem
        {
            long lastRowId;
            InsertAll(models, out lastRowId);
        }

        public static void InsertAll<T>(List<T> db_models, out long lastRowId) where T : Elem
        {
            lastRowId = 0;

            if (db_models.Count > 0)
            {
                try
                {
                    if (_db.InsertAll(db_models, out lastRowId) <= 0)
                    {
                        Log.Logger.Print("SQl InsertAll failed", true);
                    }
                }
                catch (Exception ex)
                {
                    Log.Logger.Print(ex, true);
                }
            }
        }

        public static void CreateTable<T>() where T : Elem
        {
            try
            {
                _db.CreateTable<T>();
            }
            catch (Exception ex)
            {
                Log.Logger.Print(ex, true);
            }
		}

        public static T SelectFirst<T>() where T : new()
        {
            return SelectAll<T>().FirstOrDefault();
        }

        public static List<T> SelectAll<T>() where T : new()
        {
            try
            {
                return new List<T>(from w in _db.Table<T>() select w);
            }
            catch (Exception ex)
            {
                Log.Logger.Print(ex, true);
                return default(List<T>);
            }
		}

        public static List<T> Select<T>(String sql) where T : new()
        {
            return _db.Query<T>(sql);
        }

        public static bool IsDbEmpty()
        {
            try
            {
                List<Vector> vs = _db.Query<Vector>("SELECT * FROM Vector");
                return vs == null;
            }
            catch (SQLiteException ex)
            {
                Log.Logger.Print(ex, true);
                return true;
            }
            
        }

        private static void Close()
        {
            if (_db != null)
            {
                _db.Close();
            }
        }

        private static void Dispose()
        {
            if (_db != null)
            {
                _db.Dispose();
                _db = null;
            }
        }
    }
}