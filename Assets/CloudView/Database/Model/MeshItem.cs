﻿using SimpleSQL;

namespace Database.Model
{
    public class MeshItem : Elem
    {
        [MaxLength(36)]
        public string Min { get; set; }

        [MaxLength(36)]
        public string Max { get; set; }

        [MaxLength(36)]
        public string Center { get; set; }

        [MaxLength(36)]
        public string FileName { get; set; }

        [MaxLength(36)]
        public string GroupId { get; set; }
    }
}