﻿
namespace Database.Model
{
    public class Vector : Elem
    {
        public float x { get; set; }

        public float y { get; set; }

        public float z { get; set; }
    }
}
