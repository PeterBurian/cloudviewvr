﻿using SimpleSQL;

namespace Database.Model
{
    public class PtxGroupHeader : Elem
    {
        public int col { get; set; }

        public float row { get; set; }

        [MaxLength(36)]
        public string Position { get; set; }


        [MaxLength(36)]
        public string AxisX { get; set; }

        [MaxLength(36)]
        public string AxisY { get; set; }

        [MaxLength(36)]
        public string AxisZ { get; set; }


        [MaxLength(36)]
        public string TransformMatrix { get; set; }

        [MaxLength(36)]
        public string Rotation { get; set; }

        [MaxLength(36)]
        public string TransformCoor { get; set; }

        [MaxLength(36)]
        public string Precision { get; set; }

        [MaxLength(36)]
        public string GroupId { get; set; }

        public float Min_X { get; set; }

        public float Min_Y { get; set; }

        public float Min_Z { get; set; }

        public float Max_X { get; set; }

        public float Max_Y { get; set; }

        public float Max_Z { get; set; }
    }
}