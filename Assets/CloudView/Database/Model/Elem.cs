﻿using SimpleSQL;

namespace Database.Model
{
    public class Elem
    {
        [Indexed, MaxLength(36)]
        public string id { get; set; }

        long created { get; set; }

        long deleted { get; set; }
    }
}
