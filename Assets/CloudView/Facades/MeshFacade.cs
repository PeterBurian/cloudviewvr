﻿using Database;
using Database.Model;
using Models;
using Scripts.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Utilities;

namespace Scripts.Managers
{
    public class MeshFacade
    {
        public class Util
        {
            public bool visibility;
            public Density density;
            public bool isDensityChanged;

            public Util(bool visibility, Density density)
            {
                this.visibility = visibility;
                this.density = density;
                isDensityChanged = false;
            }

            public Util(bool visibility, Density density, bool isDensityChanged)
            {
                this.visibility = visibility;
                this.density = density;
                this.isDensityChanged = isDensityChanged;
            }
        }

        private static MeshFacade instance;

        private Dictionary<String, MeshModel> models;

        private Dictionary<String, Util> visibles; //Filename, <visibility,density>

        public delegate void Mthd(MeshModel model);

        public static MeshFacade GetFacade()
        {
            if (instance == null)
            {
                instance = new MeshFacade();
            }
            return instance;
        }

        private MeshFacade()
        {
            models = new Dictionary<string, MeshModel>();
            visibles = new Dictionary<string, Util>();
        }

        public void AddMesh(MeshModel model)
        {
            if (!models.ContainsKey(model.fileName))
            {
                models.Add(model.fileName, model);
            }
        }

        public void Loop(Mthd mthd)
        {
            foreach (KeyValuePair<String, MeshModel> pair in models)
            {
                mthd(pair.Value);
            }
        }

        public void Load()
        {
            Dictionary<String, Vector3> vectors = new Dictionary<string, Vector3>();
            List<Vector> vecs = SqliteAdapter.SelectAll<Vector>();

            for (int i = 0; i < vecs.Count; i++)
            {
                vectors.Add(vecs[i].id, new Vector3(vecs[i].x, vecs[i].y, vecs[i].z));
            }

            vecs.Clear();
            vecs = null;

            List<MeshItem> meshes = SqliteAdapter.SelectAll<MeshItem>();

            for (int i = 0; i < meshes.Count; i++)
            {
                Vector3 min = vectors[meshes[i].Min];
                Vector3 max = vectors[meshes[i].Max];
                Vector3 center = vectors[meshes[i].Center];

                MeshModel model = new MeshModel(center, min, max, meshes[i].FileName);
                AddMesh(model);
            }
            vectors.Clear();
            meshes.Clear();
        }

        public SortedList<float, String> GetVisiblesByDistance(Vector3 from)
        {
            //Use sorted list to not crash when same distance are calculated
            SortedList<float, String> mshs = new SortedList<float, String>();

            foreach (KeyValuePair<String, Util> pair in visibles)
            {
                if (pair.Value.visibility)
                {
                    MeshModel model = models[pair.Key];
                    float dist = Vector3.Distance(from, model.center);
                    mshs.Add(dist, model.fileName);
                }
            }
            return mshs;
        }

        public List<String> GetInvisibles(bool delete)
        {
            List<String> invisibles = new List<String>();

            foreach (KeyValuePair<String, Util> pair in visibles)
            {
                if (!pair.Value.visibility)
                {
                    invisibles.Add(pair.Key);
                }
            }

            if (delete)
            {
                for (int i = 0; i < invisibles.Count; i++)
                {
                    visibles.Remove(invisibles[i]);
                }
            }
            return invisibles;
        }

        public void ResetUtils()
        {
            List<String> keys = visibles.Keys.ToList();

            for (int i = 0; i < keys.Count; i++)
            {
                visibles[keys[i]] = new Util(false, visibles[keys[i]].density, false);
            }
        }

        public List<String> GetNewVisiblesPath(List<String> fileNames)
        {
            String folder = Paths.Data();

            for (int i = 0; i < fileNames.Count; i++)
            {
                String path = Path.Combine(folder, fileNames[i] + ".bin");
                fileNames[i] = path;
            }
            return fileNames;
        }

        public List<String> GetVisiblesPath()
        {
            List<String> filePaths = new List<String>();
            String folder = Paths.Data();

            foreach (KeyValuePair<String, Util> pair in visibles)
            {
                if (pair.Value.visibility)
                {
                    String path = Path.Combine(folder, pair.Key + ".bin");
                    filePaths.Add(path);
                }
            }

            return filePaths;
        }

        public MeshModel Get(String fileName)
        {
            if (models.ContainsKey(fileName))
            {
                return models[fileName];
            }
            return null;
        }

        public Dictionary<String, Density> GetChangedDensitiesPath()
        {
            Dictionary<String, Density> result = new Dictionary<string, Density>();
            String folder = Paths.Data();

            foreach (KeyValuePair<String, Util> pair in visibles)
            {
                if (pair.Value.visibility && pair.Value.isDensityChanged)
                {
                    String path = Path.Combine(folder, pair.Key + ".bin");
                    result.Add(path, pair.Value.density);
                }
            }

            return result;
        }

        public bool Add2Visibles(String fileName, Density density)
        {
            bool isContains = visibles.ContainsKey(fileName);

            if (isContains && density != Density.NONE && visibles[fileName].density != density)
            {
                visibles[fileName].visibility = true;
                visibles[fileName].density = density;
                visibles[fileName].isDensityChanged = true;

                return false;
            }
            else if (isContains)
            {
                visibles[fileName].visibility = true;
                return false;
            }
            else if (density != Density.NONE)
            {
                //It is a new item
                visibles.Add(fileName, new Util(true, density));
                return true;
            }
            else
            {
                //It is a new item
                visibles.Add(fileName, new Util(true, density));
                return true;
            }
        }

        public bool RemoveFromVisibles(String fileName)
        {
            if (visibles.ContainsKey(fileName))
            {
                visibles.Remove(fileName);
                return true;
            }
            return false;
        }
    }
}
