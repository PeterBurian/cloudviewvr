﻿using CloudView.Enums;
using Assets.CloudView.Sensors;
using CloudView.Components;
using CloudView.Controller;
using CloudView.Events;
using UnityEngine;
using Utilities;

namespace Assets.CloudView.Components
{
    [RequireComponent(typeof(MeasureComponent))]
    [RequireComponent(typeof(FlagsComponent))]
    public class ComponentController : MonoBehaviour, IControllPressed, IJoyMode
    {
        [SerializeField]
        private Combo ComboType;

        MeasureComponent measureComponent = null;
        FlagsComponent flagComponent = null;

        public void Awake()
        {
            measureComponent = gameObject.GetComponent<MeasureComponent>();
            flagComponent = gameObject.GetComponent<FlagsComponent>();

            ComboType = Combo.MEASURE;

            EventBus.Instance.Register<IControllPressed>(gameObject);
        }

        public void OnDestroy()
        {
            EventBus.Instance.Unregister(gameObject);
        }

        #region Joy button pressed

        public void A_Pressed()
        {
            if (ComboType == Combo.MEASURE)
            {
                if (measureComponent != null && measureComponent.enabled)
                {
                    measureComponent.Shot();
                }
            }
            else if (ComboType == Combo.LIFT)
            {
                EventBus.Instance.Post<IMarch>((e, f) => e.Lift(true));
            }
        }

        public void B_Pressed()
        {
            if (ComboType == Combo.MEASURE)
            {
                measureComponent.enabled = !measureComponent.enabled;
            }
            else if (ComboType == Combo.LIFT)
            {
                EventBus.Instance.Post<IMarch>((e, f) => e.Lift(false));
            }
        }

        public void C_Pressed()
        {
            if (ComboType == Combo.SHADERS)
            {
                MeshBuilder.ChangeColorMode();
            }
        }

        public void D_Pressed()
        {
            if (ComboType == Combo.TELEPORT)
            {
                Vector3 nextPosition = flagComponent.GetNext();
                EventBus.Instance.Post<IMarch>((e, f) => e.TeleportNext(nextPosition));
            }
        }

        public void Step2Next()
        {
            if (ComboType == Combo.SHADERS)
            {
                ComboType = Combo.MOVE;
            }
            else
            {
                ComboType += 1;
            }
        }

        #endregion
    }
}
