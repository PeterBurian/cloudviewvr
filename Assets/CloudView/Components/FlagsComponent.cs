﻿using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Assets.CloudView.Components
{
    public class FlagsComponent : MonoBehaviour
    {
        private List<Vector3> flagPositions;
        private int index;

        private void Awake()
        {
            flagPositions = new List<Vector3>();
            index = -1;

            CollectFlags();
        }

        public Vector3 GetNext()
        {
            int count = flagPositions.Count;
            int idx = ++index % count;
            return flagPositions[idx];
        }

        private void CollectFlags()
        {
            GameObject flags = GameObject.Find(Strings.Flags);

            if (flags != null)
            {
                foreach (Transform child in flags.transform)
                {
                    flagPositions.Add(child.position);
                }
            }
        }
    }
}
