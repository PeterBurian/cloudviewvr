﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CloudView.Components
{
    public class MeasureComponent : MonoBehaviour
    {
        private static int layerMask = 0;
        private Camera camerMain;
        private List<GameObject> shots;
        private GameObject parent;
        GameObject crossHair;

        public void Start()
        {
            layerMask = 1 << 15;
            camerMain = GameObject.Find("Main Camera").GetComponent<Camera>();
            shots = new List<GameObject>();

            GameObject go = GameObject.Find("Measures");
            if (go != null)
            {
                GameObject.Destroy(go);
            }

            parent = new GameObject("Measures");

            crossHair = GameObject.Find("CrossHair");
        }

        public void OnEnable()
        {
            SwitchGUI(true);
        }

        public void OnDisable()
        {
            SwitchGUI(false);
        }

        public void Shot()
        {
            Vector3 rayDirection = camerMain.transform.TransformDirection(Vector3.forward);
            Vector3 rayStart = camerMain.transform.position + rayDirection;

            RaycastHit[] hits = Physics.RaycastAll(rayStart, rayDirection, float.PositiveInfinity, layerMask);

            if (hits.Length > 0)
            {
                ProcessShot(hits, rayStart, rayDirection);
            }

            Debug.DrawRay(rayStart, rayDirection, Color.red, 300);
        }

        private void ProcessShot(RaycastHit[] hits, Vector3 origo, Vector3 dir)
        {
            Dictionary<float, Vector3> results = new Dictionary<float, Vector3>();
            float minDist = .0f;
            Vector3 vec3 = Vector3.zero;

            for (int i = 0; i < hits.Length; i++)
            {
                MeshFilter filter = hits[i].transform.gameObject.GetComponent<MeshFilter>();
                Vector3[] verts = filter.mesh.vertices;

                KeyValuePair<float, Vector3> res = CheckVertsInMesh(verts, origo, hits[i].point);

                if (res.Key < .5f)
                {
                    results.Add(res.Key, res.Value);
                    float dist = Vector3.Distance(res.Value, origo);

                    if (minDist > dist || minDist == 0)
                    {
                        minDist = dist;
                        vec3 = res.Value;
                    }
                }
            }
            AddShot(vec3);
        }

        private void AddShot(Vector3 vec)
        {
            GameObject go = null;
            int shotCount = shots.Count;

            if (shotCount == 2)
            {
                //Delete all previous shots
                for (int i = 0; i < shotCount; i++)
                {
                    GameObject.Destroy(shots[i]);
                }
                shots.Clear();
            }

            if (shots.Count == 0)
            {
                go = LoadShotFromPrefab(vec, "Prefabs/SignWithoutText");
                shots.Add(go);
            }
            else
            {
                Vector3 v1 = shots[0].transform.position;
                Vector3 v2 = vec;

                float distance = Vector3.Distance(v1, v2);

                go = LoadShotFromPrefab(vec, "Prefabs/SignWithText");

                GameObject txtGo = GameObject.Find("DistanceTxt");
                TextMesh txtMesh = txtGo.GetComponent<TextMesh>();
                txtMesh.text = distance.ToString("0.##") + " m";

                shots.Add(go);
            }
        }

        private GameObject LoadShotFromPrefab(Vector3 vec, String path)
        {
            GameObject go = Instantiate(Resources.Load(path)) as GameObject; ;
            go.transform.position = vec;
            go.name = "Shot";
            go.transform.parent = parent.transform;

            return go;
        }

        private KeyValuePair<float, Vector3> CheckVertsInMesh(Vector3[] verts, Vector3 origo, Vector3 dir)
        {
            float mindist = 0;
            Vector3 cross = Vector3.zero;
            Vector3 point = Vector3.zero;

            for (int i = 0; i < verts.Length; i++)
            {
                float dist = DistanceFromLine(origo, dir, verts[i], out cross);
                if (mindist == 0 || dist < mindist)
                {
                    mindist = dist;
                    point = verts[i];
                }
            }
            return new KeyValuePair<float, Vector3>(mindist, point);
        }

        private float DistanceFromLine(Vector3 lineStart, Vector3 lineEnd, Vector3 point, out Vector3 cross)
        {
            cross = NearestPointOnLine(lineStart, lineEnd, point, false);
            return Vector3.Distance(cross, point);
        }

        private Vector3 NearestPointOnLine(Vector3 lineStart, Vector3 lineEnd, Vector3 pt, bool isSegment)
        {
            float dot = Vector3.Dot(lineEnd - lineStart, lineEnd - lineStart);

            if (dot == 0.0)
            {
                return lineStart;
            }
                
            float denom = Vector3.Dot(pt - lineStart, lineEnd - lineStart) / dot;

            if (isSegment && denom < 0.0)
            {
                return lineStart;
            }
            else if (isSegment && denom > 1.0)
            {
                return lineEnd;
            }
            else
            {
                return lineStart + (lineEnd - lineStart) * denom;
            }
        }

        private void SwitchGUI(bool isEnable)
        {
            if (parent != null)
            {
                parent.SetActive(isEnable);
            }

            if (crossHair != null)
            {
                crossHair.SetActive(isEnable);
            }
        }
    }
}
