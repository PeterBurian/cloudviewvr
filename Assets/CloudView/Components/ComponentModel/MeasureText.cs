﻿using CloudView.Controller;
using CloudView.Events;
using UnityEngine;

namespace CloudView.Components
{
    public class MeasureText : MonoBehaviour, IMove
    {
        private static Transform player = null;

        private void Awake()
        {
            EventBus.Instance.Register<IMove>(this.gameObject);

            if (player == null)
            {
                player = GameObject.Find("FPSController").transform;
            }
        }

        public void OnDestroy()
        {
            EventBus.Instance.Unregister(gameObject);
        }

        public void PlayerMoved()
        {
            transform.LookAt(player);
            transform.Rotate(new Vector3(0, 180, 0));
        }
    }
}
