﻿using UnityEngine;

namespace Models
{
    public class CloudPoint
    {
        private Vector3 point;
        private Color color;
        private float intensity;

        public Vector3 Vector
        {
            get { return point; }
            set { point = value; }
        }

        public Color Color
        {
            get { return color; }
            set { color = value; }
        }

        public float Intensity
        {
            get { return intensity; }
            set { intensity = value; }
        }

        public bool IsInvalid
        {
            get
            {
                return point.x == .0f && point.y == .0f && point.z == .0f && intensity == 0.5f;
            }
        }         

        #region constructors

        public CloudPoint()
        {
            point = Vector3.zero;
            color = Color.cyan;
        }

        public CloudPoint(Vector3 pt)
        {
            point = pt;
            color = Color.cyan;
            intensity = 0.5f;
        }

        public CloudPoint(Vector3 pt, Color clr)
        {
            point = pt;
            color = clr;
            intensity = 0.5f;
        }

        public CloudPoint(Vector3 point, Color color, float intensity)
        {
            this.point = point;
            this.color = color;
            this.intensity = intensity;
        }

        #endregion
    }
}