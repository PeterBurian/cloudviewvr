﻿using Database.Model;
using System;
using UnityEngine;
using Utilities;

namespace Models.Ptx
{
    public class ScanHeader
    {
        public int col;
        public int row;

        public Vector3 pos;
        public Vector3 axisX;
        public Vector3 axisY;
        public Vector3 axisZ;

        public Vector3 transformMatrix;
        public Vector3 rotation;
        public Vector3 transformCoor;
        public Vector3 precision;

        public ScanHeader()
        {
            col = 0;
            row = 0;

            pos = Vector3.zero;
            axisX = Vector3.zero;
            axisY = Vector3.zero;
            axisZ = Vector3.zero;

            transformMatrix = Vector3.zero;
            rotation = Vector3.zero;
            transformCoor = Vector3.zero;
            precision = Vector3.zero;
        }

        public ScanHeader(int col, int row)
        {
            this.col = col;
            this.row = row;

            pos = Vector3.zero;
            axisX = Vector3.zero;
            axisY = Vector3.zero;
            axisZ = Vector3.zero;

            transformMatrix = Vector3.zero;
            rotation = Vector3.zero;
            transformCoor = Vector3.zero;
            precision = Vector3.zero;
        }

        public ScanHeader(int col, int row, Vector3 pos, Vector3 axisX, Vector3 axisY, Vector3 axisZ)
        {
            this.col = col;
            this.row = row;

            this.pos = pos;
            this.axisX = axisX;
            this.axisY = axisY;
            this.axisZ = axisZ;

            transformMatrix = Vector3.zero;
            rotation = Vector3.zero;
            transformCoor = Vector3.zero;
            precision = Vector3.zero;
        }

        public ScanHeader(int col, int row, Vector3 pos, Vector3 axisX, Vector3 axisY, Vector3 axisZ, Vector3 transformMatrix, Vector3 rotation, Vector3 transformCoor, Vector3 precision)
        {
            this.col = col;
            this.row = row;

            this.pos = pos;
            this.axisX = axisX;
            this.axisY = axisY;
            this.axisZ = axisZ;

            this.transformMatrix = transformMatrix;
            this.rotation = rotation;
            this.transformCoor = transformCoor;
            this.precision = precision;
        }

        //ToString not the best beacuse I see it in debugger
        public string ConvertToString()
        {
            String header = col.ToString() + Environment.NewLine
                + row.ToString() + Environment.NewLine
                + Vector2String(pos, "") + Environment.NewLine
                + Vector2String(axisX, "") + Environment.NewLine
                + Vector2String(axisY, "") + Environment.NewLine
                + Vector2String(axisZ, "") + Environment.NewLine
                + Vector2String(transformMatrix, "0") + Environment.NewLine
                + Vector2String(rotation, "0") + Environment.NewLine
                + Vector2String(transformCoor, "0") + Environment.NewLine
                + Vector2String(precision, "1");

            return header;
        }

        private String Vector2String(Vector3 vec, String extra)
        {
            if (String.IsNullOrEmpty(extra))
            {
                return String.Format(vec.x + " " + vec.y + " " + vec.z);
            }
            else
            {
                return String.Format(vec.x + " " + vec.y + " " + vec.z + " "+ extra);
            }
        }
    }
}