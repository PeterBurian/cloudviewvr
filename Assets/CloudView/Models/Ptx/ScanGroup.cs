﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utilities.Extensions;

namespace Models.Ptx
{
    public class ScanGroup
    {
        private static int COL_ROW_LIMIT = 250;
        private ScanHeader header;
        private CloudPoint[,] points;
        private String groupId;
        private String shortGuid;
        private String projectId;

        public Vector3 min;
        public Vector3 max;

        public int NumberOfPoints
        {
            get
            {
                return header.col * header.row;
            }
        }

        public ScanHeader Header
        {
            get
            {
                return header;
            }
        }

        public String GetShortGuid()
        {
            if (String.IsNullOrEmpty(shortGuid))
            {
                shortGuid = groupId.Substring(0, 8);
            }
            return shortGuid;
        }

        public static int GetColRowLimit
        {
            get
            {
                return COL_ROW_LIMIT;
            }
            private set { }
        }

        private String GetGroupId(int col, int row)
        {
            int colNum = Mathf.CeilToInt(col / COL_ROW_LIMIT);
            int rowNum = Mathf.CeilToInt(row / COL_ROW_LIMIT);

            return String.Format("{0}_{1}_{2}_{3}", projectId, GetShortGuid(), colNum, rowNum);
        }

        #region constructors

        public ScanGroup()
        {
            header = new ScanHeader();
            groupId = Guid.NewGuid().ToString();
            projectId = String.Empty;
        }

        public ScanGroup(String projectId)
        {
            this.header = new ScanHeader();
            this.groupId = Guid.NewGuid().ToString();
            this.projectId = projectId;
        }

        public ScanGroup(String projectId, ScanHeader header)
        {
            this.header = header;
            this.points = new CloudPoint[header.col, header.row];
            this.groupId = Guid.NewGuid().ToString();
            this.projectId = projectId;
        }

        public ScanGroup(String projectId, ScanHeader header, CloudPoint[,] points)
        {
            this.header = header;
            this.points = points;
            this.groupId = Guid.NewGuid().ToString();
            this.projectId = projectId;
        }

        #endregion

        public void AddPoint(int colNum, int rowNum, CloudPoint point)
        {
            if (colNum < header.col && rowNum < header.row)
            {
                points[colNum, rowNum] = point;
            }
        }

        public void AddPointWithoutCheck(int colNum, int rowNum, CloudPoint point)
        {
            points[colNum, rowNum] = point;
        }

        public void AddValidPoint(int colNum, int rowNum, CloudPoint point)
        {
            if (!point.IsInvalid)
            {
                AddPointWithoutCheck(colNum, rowNum, point);
            }
        }

        public void AddPoints(CloudPoint[,] pointMatrix)
        {
            int colNum = pointMatrix.GetLength(0);
            int rowNum = pointMatrix.GetLength(1);

            if (colNum == header.col && rowNum == header.row)
            {
                points = pointMatrix.Clone() as CloudPoint[,];
            }
        }

        public Dictionary<String, List<CloudPoint>> SetPointsForGroup()
        {
            Dictionary<String, List<CloudPoint>> result = new Dictionary<String, List<CloudPoint>>();

            int limit = COL_ROW_LIMIT * COL_ROW_LIMIT;
            int l1 = points.GetLength(0);
            int l2 = points.GetLength(1);

            String grId = String.Empty;

            for (int i = 0; i < l1; i++)
            {
                for (int j = 0; j < l2; j++)
                {
                    CloudPoint pt = points[i, j];
                    if (pt != null)
                    {
                        pt = CloudPointExtension.Convert(header, pt);

                        if ((!result.ContainsKey(grId)) || result[grId].Count > limit)
                        {
                            grId = GetGroupId(i, j);
                            result[grId] = new List<CloudPoint>() { pt };
                        }
                        else
                        {
                            result[grId].Add(pt);
                        }
                    }
                }
            }
            return result;
        }
    }
}