﻿using Assets.CloudView;
using Scripts.Enums;
using Scripts.Managers;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Models
{
    public class MeshModel
    {
        public Vector3 center;
        public Vector3 min;
        public Vector3 max;
        public String fileName;
        public bool isVisible;
        public int visibleRate; //How many points are used for this mesh

        #region constructors

        public MeshModel()
        {
            center = Vector3.zero;
            min = Vector3.zero;
            max = Vector3.zero;

            fileName = String.Empty;
            isVisible = false;
        }

        public MeshModel(Vector3 center, Vector3 min, Vector3 max)
        {
            this.center = center;
            this.min = min;
            this.max = max;

            fileName = String.Empty;
            isVisible = false;
        }

        public MeshModel(Vector3 center, Vector3 min, Vector3 max, String fName)
        {
            this.center = center;
            this.min = min;
            this.max = max;

            fileName = fName;
            isVisible = false;
        }

        #endregion

        public Bounds GetBounds()
        {
            Vector3 extents = max - center;
            Vector3 size = extents * 2;
            //Vector3 size = extents * 8;

            Vector3 newCenter = new Vector3(center.x, center.y + Configs.verticalPush, center.z);

            Bounds bounds = new Bounds(newCenter, size);
            return bounds;
        }

        public Bounds GetBoundsChangeYZ()
        {
            Vector3 extents = max - center;
            Vector3 size = extents * 2;
            //Vector3 size = extents * 8;

            Vector3 newCenter = new Vector3(center.x, center.y + Configs.verticalPush, center.z);

            Bounds bounds = new Bounds(newCenter, size);
            return bounds;
        }

        public List<Vector3> GetBoundsVectors()
        {
            List<Vector3> corners = new List<Vector3>();

            Vector3 extents = max - center;
            Vector3 newCenter = new Vector3(center.x, center.y + Configs.verticalPush, center.z);

            Vector3[] crnrs = GetCorners(newCenter, extents);
            corners.AddRange(crnrs);

            return corners;
        }

        public Vector2[] GetBoundsBottomVectors()
        {
            List<Vector3> corners = GetBoundsVectors();

            Vector2[] boundBottom = new Vector2[4]
            {
                new Vector2(corners[2].x, corners[2].y),
                new Vector2(corners[3].x, corners[3].y),
                new Vector2(corners[6].x, corners[6].y),
                new Vector2(corners[7].x, corners[7].y)
            };

            return boundBottom;
        }

        private Vector3[] GetCorners(Vector3 center, Vector3 extents)
        {
            Vector3[] corners = new Vector3[8];

            corners[0] = center + new Vector3(extents.x, extents.y, extents.z);
            corners[1] = center + new Vector3(extents.x, extents.y, -extents.z);
            corners[2] = center + new Vector3(extents.x, -extents.y, extents.z);
            corners[3] = center + new Vector3(extents.x, -extents.y, -extents.z);

            corners[4] = center + new Vector3(-extents.x, extents.y, extents.z);
            corners[5] = center + new Vector3(-extents.x, extents.y, -extents.z);
            corners[6] = center + new Vector3(-extents.x, -extents.y, extents.z);
            corners[7] = center + new Vector3(-extents.x, -extents.y, -extents.z);

            return corners;
        }

        public bool SetVisible(Density density)
        {
            this.isVisible = true;
            return MeshFacade.GetFacade().Add2Visibles(this.fileName, density);
        }

        public bool RemoveVisible()
        {
            this.isVisible = false;
            return MeshFacade.GetFacade().RemoveFromVisibles(this.fileName);
        }
    }
}