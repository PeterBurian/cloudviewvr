﻿using Assets.CloudView;
using Database;
using Database.Model;
using MovementEffects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using Utilities;

namespace Assets.Scripts
{
    public class CloudView : MonoBehaviour
    {
        private bool showGui = false;
        private float progress = 0;
        private new string guiText;
        private Loader loader;

        public void Awake()
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            String dbPath = Paths.DbPath();

            if (File.Exists(dbPath))
            {
                guiText = "Initialize database";
                SqliteAdapter.Init(dbPath, true);

                Log.Logger.Print("Existed db Initialized" + Environment.NewLine + "dbPath: " + dbPath);
            }

            loader = GetComponent<Loader>();

            if (!CopyDatas2Persistent())
            {
                loader.GetDensity();
                loader.LoadMeshes();
            }
        }

        public void OnApplicationQuit()
        {
            SqliteAdapter.CloseDB();
        }

        private bool CopyDatas2Persistent()
        {
            String dbPath = Paths.DbPath();
            String fromDbPath = Path.Combine(Application.streamingAssetsPath, Paths.dbName);

            if (!File.Exists(dbPath))
            {
#if UNITY_EDITOR || UNITY_STANDALONE_WIN

                File.Move(fromDbPath, dbPath);

#elif UNITY_ANDROID
                WWW reader = new WWW(fromDbPath);
                while ( ! reader.isDone) {}

                System.IO.File.WriteAllBytes(dbPath, reader.bytes);
#endif
                SqliteAdapter.Init(dbPath, true);
                Log.Logger.Print("Moved db Initialized");
            }

            bool startCopy = false;
            String datafolder = Paths.Data();

            if (!Directory.Exists(datafolder))
            {
                Directory.CreateDirectory(datafolder);
                startCopy = true;
            }
            else
            {
                String[] files = Directory.GetFiles(datafolder, "*.bin");

                if (files.Length == 0)
                {
                    startCopy = true;
                }
            }

            // Get the count of the meshItem table

            // Reproduct the fileNames
            // Copy them in a loop

            //Must find out something. The db is not initialized yet, so I can not use it.
            //int piece = SqliteAdapter.SelectAll<MeshItem>().Count;

            if (startCopy)
            {
                showGui = true;
                IEnumerator<float> _copy = _StartCopy();
                Timing.RunCoroutine(_copy);
            }
            return startCopy;
        }

        private List<String> GetDataFiles()
        {
            return SqliteAdapter.SelectAll<MeshItem>().Select(a => a.FileName).ToList();
        }

        private IEnumerator<float> _StartCopy()
        {
            List<String> files = GetDataFiles();

            int piece = files.Count;

            for (int i = 0; i < piece; i++)
            {
                String fileName = Path.GetFileName(files[i]);
                String from = Path.Combine(Paths.StreamingData(), files[i] + Paths.dataExtension);
                String to = Path.Combine(Paths.Data(), fileName + Paths.dataExtension);

#if UNITY_EDITOR || UNITY_STANDALONE_WIN

                File.Move(from, to);
#elif UNITY_ANDROID
                    WWW reader = new WWW(from);
                    while ( ! reader.isDone) {}

                    System.IO.File.WriteAllBytes(to, reader.bytes);
#endif

                float rate = ((i * 1.0f) / piece);
                progress = rate;
                float percent = rate * 100.0f;
                guiText = percent + " %";

                yield return Timing.WaitForOneFrame;
            }

            showGui = false;
            loader.LoadMeshes();
        }

        private void OnGUI()
        {
            if (showGui)
            {
                Rect rect = new Rect(Screen.width / 2 - 100, (Screen.height / 2) - 52, 200, 20);
                GUI.TextField(rect, "Copying resources");

                GUI.BeginGroup(new Rect(Screen.width / 2 - 100, Screen.height / 2, 400.0f, 20));
                GUI.Box(new Rect(0, 0, 200.0f, 20.0f), guiText);
                GUI.Box(new Rect(0, 0, progress * 200.0f, 20), "");
                GUI.EndGroup();
            }
        }
    }
}