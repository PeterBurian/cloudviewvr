﻿using Converter2Bin;
using Models;
using Models.Ptx;
using System;
using System.IO;
using System.Threading;
using UnityEditor;
using UnityEngine;

namespace Converter2Bin
{
    public class SeparatePtx : BasePtx
    {
        [MenuItem("Tools/Convert/Separate PTX", false, 110)]
        public static void SeparatePTXs()
        {
            String path = EditorUtility.OpenFilePanel("Add Project (.ptx)", "", "ptx");

            if (path.Length != 0)
            {
                String dataPath = Path.Combine(Application.streamingAssetsPath, "PtxCams");

                if (Directory.Exists(dataPath)) Directory.Delete(dataPath);
                Directory.CreateDirectory(dataPath);

                SeparatePtx sptx = new SeparatePtx();

                sptx.SeparateByCams(path, dataPath);
            }
        }

        private void SeparateByCams(String filePath, String outFolder)
        {
            if (!String.IsNullOrEmpty(filePath) && File.Exists(filePath))
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    String prjName = "PTX_Cams";
                    int counter = 1;

                    while (!reader.EndOfStream)
                    {
                        String fileName = prjName + "_" + counter +".ptx";
                        String outFilePath = Path.Combine(outFolder, fileName);

                        if (outFilePath != null)
                        {
                            ScanHeader header = ReadPtxHeader(reader);

                            using (FileStream stream = File.Create(outFilePath))
                            {
                                using (StreamWriter writer = new StreamWriter(stream))
                                {
                                    writer.WriteLine(header.ConvertToString());

                                    for (int i = 0; i < header.col; i++)
                                    {
                                        for (int j = 0; j < header.row; j++)
                                        {
                                            String line = reader.ReadLine();
                                            writer.WriteLine(line);
                                        }
                                    }
                                }
                            }
                            counter++;
                        }
                    }
                }
                Debug.Log("Finished");
            }
        }

        protected override string GetHeaderText()
        {
            return "PTX Settings";
        }

        protected override string GetButtonText()
        {
            return "Select ptx";
        }

        protected override void DoWork() { }
    }
}
