﻿using Database;
using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Utilities;

namespace Assets.Editor.Converter2Bin
{
    public class MenuItems : EditorToolBaseWindow
    {
        [MenuItem("Tools/Convert/OFF Converter")]
        public static void OffConverter()
        {
            String path = EditorUtility.OpenFilePanel("Add Project (.off)", "", "off");

            if (path.Length != 0)
            {
                System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
                stopWatch.Start();

                DbModelHandler.CreateDatabase();

                String dataPath = Path.Combine(Application.streamingAssetsPath, "Data");

                if (!Directory.Exists(dataPath))
                {
                    Directory.CreateDirectory(dataPath);
                }

                String tName = Path.GetFileNameWithoutExtension(path) + ".bin";
                String target = Path.Combine(dataPath, tName);

                ConvertOff(path, target);

                SqliteAdapter.CloseDB();

                stopWatch.Stop();
                Debug.Log("Off conversion time: " + stopWatch.ElapsedMilliseconds.ToString());
            }
        }

        [ExecuteInEditMode]
        private static void ConvertOff(String input, String target)
        {
            bool invertYZ = true;
            int numPoints;

            if (File.Exists(input))
            {
                DeleteFileIfExists(target);

                StreamReader sr = new StreamReader(input);
                sr.ReadLine();
                String[] buffer = sr.ReadLine().Split();

                numPoints = int.Parse(buffer[0]);
                Vector3[] points = new Vector3[numPoints];
                Color[] colors = new Color[numPoints];
                float scale = 1;

                //Create the conf file ???

                for (int i = 0; i < numPoints; i++)
                {
                    buffer = sr.ReadLine().Split();

                    if (!invertYZ)
                    {
                        points[i] = new Vector3(float.Parse(buffer[0]) * scale, float.Parse(buffer[1]) * scale, float.Parse(buffer[2]) * scale);
                    }
                    else
                    {
                        points[i] = new Vector3(float.Parse(buffer[0]) * scale, float.Parse(buffer[2]) * scale, float.Parse(buffer[1]) * scale);
                    }

                    if (buffer.Length >= 5)
                    {
                        colors[i] = new Color(int.Parse(buffer[3]) / 255.0f, int.Parse(buffer[4]) / 255.0f, int.Parse(buffer[5]) / 255.0f);
                    }
                    else
                    {
                        colors[i] = Color.cyan;
                    }
                }

                int numPointGroups = Mathf.CeilToInt(numPoints * 1.0f / limitPoints * 1.0f);

                for (int i = 0; i < numPointGroups - 1; i++)
                {
                    InstantiateMesh(points, colors, i, limitPoints);
                }

                InstantiateMesh(points, colors, numPointGroups - 1, numPoints - (numPointGroups - 1) * limitPoints);
            }
        }

        private static void InstantiateMesh(Vector3[] points, Color[] colors, int id, int nPoints)
        {
            String meshName = Paths.ProjectName() + id;

            Vector3[] meshVerts = new Vector3[nPoints];
            int[] indecies = new int[nPoints];
            Color[] myColors = new Color[nPoints];

            for (int i = 0; i < nPoints; ++i)
            {
                meshVerts[i] = points[id * limitPoints + i];
                indecies[i] = i;
                myColors[i] = colors[id * limitPoints + i];
            }

            SaveMeshPoints2File(meshVerts, myColors, meshName);

            Mesh mesh = new Mesh();
            mesh.vertices = meshVerts;

            mesh.colors = myColors;
            mesh.SetIndices(indecies, MeshTopology.Points, 0);
            mesh.uv = new Vector2[nPoints];

            mesh.normals = new Vector3[nPoints];

            DbModelHandler.AddItems2Db(mesh, meshName);
        }
    }
}