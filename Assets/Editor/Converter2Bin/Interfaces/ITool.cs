﻿using System;

namespace Converter2Bin.Interfaces
{
    public interface ITool
    {
        void Convert(String filePath, String projectName);
    }
}