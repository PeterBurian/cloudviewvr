﻿using Assets.Editor.Converter2Bin;
using Database;
using Models;
using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Utilities;

namespace Converter2Bin
{
    public abstract class BaseTool : EditorToolBaseWindow
    {
        protected delegate void ConvertMthd(String filePath, String projectName);
        protected bool isInvert = true;

        protected abstract String GetHeaderText();
        protected abstract String GetButtonText();

        protected abstract void DoWork();


        protected void OnGUI()
        {
            Paths.SetUnityPaths();

            String headerTxt = GetHeaderText();
            String btnTtxt = GetButtonText();

            GUILayout.Label(headerTxt, EditorStyles.boldLabel);
            isInvert = EditorGUILayout.Toggle("Invert YZ", isInvert);

            GUIContent btnTxt = new GUIContent(btnTtxt);
            Rect rect = GUILayoutUtility.GetRect(btnTxt, GUI.skin.button, GUILayout.ExpandWidth(false));
            rect.center = new Vector2(EditorGUIUtility.currentViewWidth / 4, rect.center.y);
            if (GUI.Button(rect, btnTxt, GUI.skin.button))
            {
                DoWork();
            }
        }

        protected void CreateDbAndDataFolder(String path, ConvertMthd mthd)
        {
            try
            {
                DbModelHandler.CreateDatabase();

                String dataPath = Path.Combine(Application.streamingAssetsPath, "Data");

                if (Directory.Exists(dataPath)) Directory.Delete(dataPath);
                Directory.CreateDirectory(dataPath);

                String projectName = SetDbProject(path);
                mthd(path, projectName);
            }
            catch (Exception ex)
            {
                String error = ex.ToString();
                Debug.LogError(error);
                Log.Logger.Print("CreateDbAndDataFolder: " + error);
            }
            finally
            {
                SqliteAdapter.CloseDB();
            }
        }

        protected bool GetCloudPointVector(StreamReader reader, out CloudPoint result)
        {
            String[] buffer = reader.ReadLine().Split();
            if (buffer.Length > 6)
            {
                Vector3 vector = new Vector3(float.Parse(buffer[0]), float.Parse(buffer[1]), float.Parse(buffer[2]));
                float intensity = float.Parse(buffer[3]);
                Color color = new Color(int.Parse(buffer[4]) / 255.0f, int.Parse(buffer[5]) / 255.0f, int.Parse(buffer[6]) / 255.0f);

                result = new CloudPoint(vector, color, intensity);

                return !result.IsInvalid;
            }

            result = new CloudPoint();
            return false;
        }

        protected bool GetCloudPointVectorWithoutIntensity(StreamReader reader, out CloudPoint result)
        {
            String[] buffer = reader.ReadLine().Split();
            if (buffer.Length > 6)
            {
                Vector3 vector = new Vector3(float.Parse(buffer[0]), float.Parse(buffer[1]), float.Parse(buffer[2]));
                Color color = new Color(int.Parse(buffer[3]) / 255.0f, int.Parse(buffer[4]) / 255.0f, int.Parse(buffer[5]) / 255.0f, int.Parse(buffer[6]) / 255.0f);

                result = new CloudPoint(vector, color);

                return !result.IsInvalid;
            }

            result = new CloudPoint();
            return false;
        }

        private String SetDbProject(String path)
        {
            String tName = Path.GetFileNameWithoutExtension(path);
            DbModelHandler.SetDb_ProjectName(tName);

            return tName;
        }
    }
}
