﻿using Converter2Bin.Interfaces;
using Database;
using Models;
using Models.Ptx;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEditor;
using UnityEngine;
using Utilities;

namespace Converter2Bin
{
    public class PtxWindow : BasePtx, ITool
    {
        private System.Diagnostics.Stopwatch stopWatch;
        private bool isReadFinished;

        [MenuItem("Tools/Convert/PTX Converter", false, 100)]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(PtxWindow));
        }


        public void Convert(String filePath, String projectName)
        {
            if (!String.IsNullOrEmpty(filePath) && File.Exists(filePath))
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        ScanHeader header = ReadPtxHeader(reader);
                        ScanGroup group = new ScanGroup(projectName, header);

                        for (int i = 0; i < header.col; i++)
                        {
                            for (int j = 0; j < header.row; j++)
                            {
                                CloudPoint point = null;
                                if (GetCloudPointVector(reader, out point))
                                {
                                    group.AddValidPoint(i, j, point);
                                }
                            }
                        }

                        CreateData(group);
                        //RunOnThread(group);
                    }
                }

                isReadFinished = true;
            }
        }

        private void RunOnThread(ScanGroup group)
        {
            String datafolderPath = Path.Combine(Paths.streamingAssetsPath, "Data");

            try
            {
                Thread thread = new Thread(delegate ()
                {
                    CreateMeshFromGroup(group, datafolderPath);
                    DbModelHandler.AddHeader2Db(group, isInvert);
                });

                thread.Start();
            }
            finally
            {
                if (isReadFinished)
                {
                    stopWatch.Stop();
                    Log.Logger.Print("PTX conversion time: " + stopWatch.ElapsedMilliseconds.ToString(), true);
                }
            }
        }

        private void CreateData(ScanGroup group)
        {
            CreateMeshFromGroup(group);
            DbModelHandler.AddHeader2Db(group, isInvert);
        }

        private void CreateMeshFromGroup(ScanGroup group, String datafolderPath=null)
        {
            Dictionary<String, List<CloudPoint>> groups = group.SetPointsForGroup();

            Vector3 groupMin = Vector3.zero;
            Vector3 groupMax = Vector3.zero;

            int idx = 0;

            foreach (KeyValuePair<String, List<CloudPoint>> item in groups)
            {
                String meshName = item.Key;

                if (item.Value.Count > 3)
                {
                    Vector3 min = Vector3.zero;
                    Vector3 max = Vector3.zero;

                    for (int i = 0; i < item.Value.Count; i++)
                    {
                        Vector3 pt = item.Value[i].Vector;

                        if (i == 0)
                        {
                            max.x = min.x = pt.x;
                            max.y = min.y = pt.y;
                            max.z = min.z = pt.z;

                            if (idx == 0)
                            {
                                groupMin.x = max.x;
                                groupMin.y = max.y;
                                groupMin.z = max.z;
                                idx++;
                            }
                        }
                        else
                        {
                            min.x = pt.x < min.x ? pt.x : min.x;
                            max.x = pt.x > max.x ? pt.x : max.x;

                            groupMin.x = min.x < groupMin.x ? min.x : groupMin.x;
                            groupMax.x = max.x < groupMax.x ? max.x : groupMax.x;

                            min.y = pt.y < min.y ? pt.y : min.y;
                            max.y = pt.y > max.y ? pt.y : max.y;

                            groupMin.y = min.y < groupMin.y ? min.y : groupMin.y;
                            groupMax.y = max.y < groupMax.y ? max.y : groupMax.y;

                            min.z = pt.z < min.z ? pt.z : min.z;
                            max.z = pt.z > max.z ? pt.z : max.z;

                            groupMin.z = min.z < groupMin.z ? min.z : groupMin.z;
                            groupMax.z = max.z < groupMax.z ? max.z : groupMax.z;
                        }
                    }

                    bool hasDatafolder = !String.IsNullOrEmpty(datafolderPath);

                    if (hasDatafolder)
                    {
                        SaveMeshPoints2File(item.Value.ToArray(), meshName, datafolderPath, isInvert);
                    }
                    else
                    {
                        SaveMeshPoints2File(item.Value.ToArray(), meshName, isInvert);
                    }

                    String groupId = group.GetShortGuid();
                    DbModelHandler.AddItems2Db(min, max, meshName, groupId, isInvert);
                }
            }

            group.min = groupMin;
            group.max = groupMax;
        }

        protected override string GetHeaderText()
        {
            return "PTX Settings";
        }

        protected override string GetButtonText()
        {
            return "Select ptx";
        }

        protected override void DoWork()
        {
            isReadFinished = false;

            String path = EditorUtility.OpenFilePanel("Add Project (.ptx)", "", "ptx");

            if (path.Length > 0)
            {
                stopWatch = new System.Diagnostics.Stopwatch();
                stopWatch.Start();

                CreateDbAndDataFolder(path, Convert);
            }
        }
    }
}