﻿using Converter2Bin.Interfaces;
using Database;
using Models;
using Models.Ptx;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using Utilities;

namespace Converter2Bin
{
    public class PtsWindow : BaseTool, ITool
    {
        private System.Diagnostics.Stopwatch stopWatch;

        #region GUI

        [MenuItem("Tools/Convert/PTS Converter", false, 300)]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow(typeof(PtsWindow));
        }

        protected override string GetHeaderText()
        {
            return "PTS Settings";
        }

        protected override string GetButtonText()
        {
            return "Select pts";
        }

        #endregion

        protected override void DoWork()
        {
            String path = EditorUtility.OpenFilePanel("Add Project (.pts)", "", "pts");

            if (path.Length > 0 && File.Exists(path))
            {
                stopWatch = new System.Diagnostics.Stopwatch();
                stopWatch.Start();

                CreateDbAndDataFolder(path, Convert);

                stopWatch.Stop();
                Log.Logger.Print("PTS conversion time: " + stopWatch.ElapsedMilliseconds.ToString(), true);
            }
        }

        public void Convert(string filePath, string projectName)
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                String firstLine = reader.ReadLine();
                int count = int.Parse(firstLine);
                int counter = 0;
                int limit = ScanGroup.GetColRowLimit * ScanGroup.GetColRowLimit;
                int groupCounter = 0;

                List<CloudPoint> points = new List<CloudPoint>();

                for (int i = 0; i < count; i++)
                {
                    CloudPoint point = null;
                    if (GetCloudPointVectorWithoutIntensity(reader, out point))
                    {
                        counter++;

                        if (counter < limit)
                        {
                            points.Add(point);
                        }
                        else
                        {
                            StringBuilder builder = new StringBuilder(projectName);
                            builder.Append("_");
                            builder.Append(groupCounter);
                            CreateMeshFromGroup(points, builder.ToString());

                            counter = 0;
                            groupCounter++;
                            points = new List<CloudPoint>();
                        }
                    }
                }
            }
        }

        private void CreateMeshFromGroup(List<CloudPoint> points, String meshName, String datafolderPath = null)
        {
            Vector3 groupMin = Vector3.zero;
            Vector3 groupMax = Vector3.zero;

            Vector3 min = Vector3.zero;
            Vector3 max = Vector3.zero;

            int idx = 0;

            for (int i = 0; i < points.Count; i++)
            {
                Vector3 pt = points[i].Vector;

                if (i == 0)
                {
                    max.x = min.x = pt.x;
                    max.y = min.y = pt.y;
                    max.z = min.z = pt.z;

                    if (idx == 0)
                    {
                        groupMin.x = max.x;
                        groupMin.y = max.y;
                        groupMin.z = max.z;
                        idx++;
                    }
                }
                else
                {
                    min.x = pt.x < min.x ? pt.x : min.x;
                    max.x = pt.x > max.x ? pt.x : max.x;

                    groupMin.x = min.x < groupMin.x ? min.x : groupMin.x;
                    groupMax.x = max.x < groupMax.x ? max.x : groupMax.x;

                    min.y = pt.y < min.y ? pt.y : min.y;
                    max.y = pt.y > max.y ? pt.y : max.y;

                    groupMin.y = min.y < groupMin.y ? min.y : groupMin.y;
                    groupMax.y = max.y < groupMax.y ? max.y : groupMax.y;

                    min.z = pt.z < min.z ? pt.z : min.z;
                    max.z = pt.z > max.z ? pt.z : max.z;

                    groupMin.z = min.z < groupMin.z ? min.z : groupMin.z;
                    groupMax.z = max.z < groupMax.z ? max.z : groupMax.z;
                }
            }

            bool hasDatafolder = !String.IsNullOrEmpty(datafolderPath);

            if (hasDatafolder)
            {
                SaveMeshPoints2File(points.ToArray(), meshName, datafolderPath, isInvert);
            }
            else
            {
                SaveMeshPoints2File(points.ToArray(), meshName, isInvert);
            }

            String groupId = Guid.NewGuid().ToString();
            DbModelHandler.AddItems2Db(min, max, meshName, groupId, isInvert);
        }
    }
}