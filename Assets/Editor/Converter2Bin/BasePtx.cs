﻿using Models;
using Models.Ptx;
using System;
using System.IO;
using UnityEngine;

namespace Converter2Bin
{
    public abstract class BasePtx : BaseTool
    {
        protected ScanHeader ReadPtxHeader(StreamReader reader)
        {
            int col = Int32.Parse(reader.ReadLine());
            int row = Int32.Parse(reader.ReadLine());

            Vector3 pos = GetVector(reader);
            Vector3 axisX = GetVector(reader);
            Vector3 axisY = GetVector(reader);
            Vector3 axisZ = GetVector(reader);

            Vector3 transformMatrix = GetVector(reader);
            Vector3 rotation = GetVector(reader);
            Vector3 transformCoor = GetVector(reader);
            Vector3 precision = GetVector(reader);

            return new ScanHeader(col, row, pos, axisX, axisY, axisZ, transformMatrix, rotation, transformCoor, precision);
        }

        protected Vector3 GetVector(StreamReader reader)
        {
            String[] buffer = reader.ReadLine().Split();
            return new Vector3(float.Parse(buffer[0]), float.Parse(buffer[1]), float.Parse(buffer[2]));
        }

        protected abstract override string GetHeaderText();

        protected abstract override string GetButtonText();

        protected abstract override void DoWork();
    }
}