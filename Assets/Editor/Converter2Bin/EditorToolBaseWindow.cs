﻿using Database;
using Database.Model;
using Handler;
using Models;
using Models.Ptx;
using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using Utilities;

namespace Assets.Editor.Converter2Bin
{
    public class EditorToolBaseWindow : EditorWindow
    {
        protected static int limitPoints = 65000;

        public static void SaveMeshPoints2File(Vector3[] points, Color[] colors, String meshName)
        {
            String datafolderPath = Path.Combine(Application.streamingAssetsPath, "Data");
            String meshPath = Path.Combine(datafolderPath, meshName + ".bin");

            BinaryHandler.Write2Bin_15(meshPath, points, colors);
        }

        protected static void SaveMeshPoints2File(CloudPoint[] points, String meshName, String datafolderPath, bool isInvert)
        {
            String meshPath = Path.Combine(datafolderPath, meshName + ".bin");
            BinaryHandler.Write2Bin_19(meshPath, points, isInvert);
        }

        protected static void SaveMeshPoints2File(CloudPoint[] points, String meshName, bool isInvert)
        {
            String datafolderPath = Path.Combine(Paths.streamingAssetsPath, "Data");
            String meshPath = Path.Combine(datafolderPath, meshName + ".bin");

            BinaryHandler.Write2Bin_19(meshPath, points, isInvert);
        }

        protected static void DeleteFileIfExists(String target)
        {
            if (File.Exists(target))
            {
                File.Delete(target);
            }
        }
    }
}
